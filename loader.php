<?php
try {
    include("assets/php/libs/URLify.php");
    include("assets/php/main.php");
    include("assets/php/config.php");
    include("assets/php/connect.php");
    include("assets/php/session.php");
    authenticate();
} catch (Exception $e) {
    echo getError($e->getMessage());
    exit;
}
?>