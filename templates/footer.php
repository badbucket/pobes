<footer class="container-fluid" id="kontakt">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>POBES - Požárně bezpečnostní služby</h2>
			</div>
		</div>
		<div class="row footer-row justify-content-between">
			<div class="col-sm-12 col-lg-8">
				<p>POBES byl založen v roce 2014 v Plzni spojením odborně způsobilých osob a beypečnostních inženýrů majících bohaté zkušenosti v oblasti bezpečnosti průmyslu a techniky požární ochrany a to jak v prevenci, tak v represi s ohledem na dlouholetou službu u Hasičského záchranného sboru České republiky. Stavíme na rodinných základech a zkušeném kolektivu, který v současné době čítá 4 stálé zaměstnance.</p>
				<p>Od počátku je naší hlavní činností poskytování komplexních služeb v oblasti požární ochrany a bezpečnosti práce dle platných zákonů, předpisů a norem.</p> 
			</div>
			<div class="col-sm-12 col-lg-3 ">
				<p><span>POBES</span> <br>Staniční 63, Plzeň 312 00</p>
				<p>Ing. Matyáš Novák <br>IČO: 88765873</p>
				<p>+420 602 552 634 <br>pobes@pobes.cz</p>
			</div>
		</div>
	</div>
	<div class="row subfooter-row">
		<div class="col">
			<span>&copy; 2015 Všechna práva vyhrazena / POBES</span>
		</div>
	</div>
</footer>