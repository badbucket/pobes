<script>
    
    $(document).ready(function () {
        obj_data = '<?php echo json_encode($questions); ?>';
        js_obj_data = JSON.parse(obj_data);
        if (js_obj_data) {
            var questionRow = $('.question-row').detach();
            var questionCounter = 0;
            $('.number-of-questions-span').text(js_obj_data.length);
            js_obj_data.forEach(function ($question, index) {
                questionRow.clone().appendTo('.question-container').find('.answer-option').each(function () {
                    var that = this;
                    var questionDiv = $(that).parent();
                    $(questionDiv).find('.question-text').text($question.question);
                    $(questionDiv).find('.question-counter').text(index + 1);
                    $(that).detach();
                    $question.answers.forEach(function ($answer) {
                        var newAnswer = $(that).clone();
                        $(newAnswer).appendTo(questionDiv).find('.answer-text').text($answer.answer);
                        $(newAnswer).find('input').attr('value', $answer.isCorrect);
                    });
                });
            });
        }
    });

    function selectAnswer(event) {
            var answerOption = event.target.closest('.answer-option');
            $(answerOption).removeClass('alert alert-danger alert-success');
            $(answerOption).find('.alert-message').text('');
            if (1 * event.target['value'] === 0) {
                $(answerOption).addClass('alert alert-danger');
                $(answerOption).find('.alert-message').text('Chybná odpověď!');
            } else if (1 * event.target['value'] === 1) {
                $(answerOption).find('.answer-option-radio').attr('disabled', '');
                $(answerOption).addClass('alert alert-success');
                $(answerOption).find('.alert-message').text('Správná odpověď!');
                setTimeout(function () {
                    if ($(answerOption).closest('.question-row').next().is('*')) {
                        $(answerOption).closest('.question-row').addClass('hidden');
                        $(answerOption).find('.answer-option-radio').attr('disabled', null);
                        $(answerOption).closest('.question-row').next().removeClass('hidden');
                    } else {
                        $.post( "/assets/php/ajax/submittest.php",
                                {
                                    testSubmittedBy: $('.userinfo-col span').first().text(),
                                    test: js_obj_data,
                                    companyId: $('select#company').val(),
                                    name: '<?php echo $_GET['testgroup']; ?>'
                                }
                        );
                        $(answerOption).closest('.container').addClass('hidden');
                        $('.test-finished-message-container .question-group-name-span').text('<?php echo $selectedQuestionGroup["name"]; ?>');
                        $('.test-finished-message-container .question-group-description-span').text('<?php echo $selectedQuestionGroup["description"]; ?>');
                        $('.test-finished-message-container').removeClass('hidden');
                    }
                }, 500);
            }
        }

</script>
</body>
</html>