<style>
.php-debug-div table {
    width: 50%;
    margin: 25px auto;
}
.php-debug-div table td {
    border: 1px solid black;
    padding: 7px;
}
</style>
<div class="php-debug-div">
    <table>
        <tr>
            <td><strong>var_dump($_SESSION)</strong></td>
            <td><?php var_dump($_SESSION); ?></td>
        </tr>
        <tr>
            <td><strong>var_dump($_SERVER)</strong></td>
            <td><?php var_dump($_SERVER); ?></td>
        </tr>
        <tr>
            <td><strong>var_dump($_POST)</strong></td>
            <td><?php var_dump($_POST); ?></td>
        </tr>
    </table>
</div>