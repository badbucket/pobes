<?php
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="cs" dir="ltr"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="cs" dir="ltr"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="cs" dir="ltr"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="cs" dir="ltr">
    <!--<![endif]-->

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php echo $title; ?></title>
    
    <meta name="author" content="blamoid">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="">

    <link href="<?php echo ASSETS . '/css/libs/bootstrap-4.0.0-beta.css' ?>" rel="stylesheet">
    <link href="<?php echo ASSETS . '/css/libs/bootstrap-4.0.0-alpha.6.min.css' ?>" rel="stylesheet">
    <link href="<?php echo ASSETS . '/css/libs/ie10-viewport-bug-workaround.css' ?>" rel="stylesheet">
    <link href="<?php echo ASSETS . '/css/libs/tablesorter/tablesorter.css' ?>" rel="stylesheet">
    <link href="<?php echo ASSETS . '/css/main.css' ?>" rel="stylesheet">

    <link href="<?php echo ROOT . '/pobes-icon.png'; ?>" rel="shortcut icon" />      
</head>
<body>

<nav class="navbar navbar-light navbar-toggleable-md">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleContainer" aria-controls="navbarsExampleContainer" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
        <a class="navbar-brand" href="<?php echo PROTOCOL.DOMAIN; ?>">
            <img alt="POBES - Kompletní servis BOZP a PO" src="<?php echo ASSETS . '/img/logo_pobes.jpg'; ?>">
        </a>
          
        <div class="navbar-text">
            <span><strong>PO</strong>žárně <strong>BE</strong>zpečnostní <strong>S</strong>lužby<br>BOZP a PO kvalitně a bez starostí</span>
        </div>

        <div class="collapse navbar-collapse" id="navbarsExampleContainer">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item bold">
                <a class="nav-link <?php if($urls[1] == $link) echo ' active';?>" href="<?php echo ROOT . '/bozp'; ?>">BOZP</a>
            </li>
            <li class="nav-item bold">
                <a class="nav-link <?php if($urls[2] == $link) echo ' active';?>" href="<?php echo ROOT . '/po'; ?>">PO</a>
            </li>
            <li class="nav-item bold">
                <a class="nav-link" href="<?php echo ROOT . '/pobes#reference'; ?>">Reference</a>
            </li>
            <li class="nav-item bold">
                <a class="nav-link" href="<?php echo ROOT . '/pobes#kontakt'; ?>">O nás / kontakt</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-blue no-mobile <?php if($urls[3] == $link) echo 'active';?>" href="<?php echo ROOT . '/dokumenty'; ?>">DOKUMENTY</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-blue no-mobile <?php if($urls[4] == $link) echo 'active';?>" href="<?php echo ROOT . '/skoleni'; ?>">ŠKOLENÍ</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
            
<?php 
if (isset($_GET['debugoff'])) {
    $_SESSION['debug'] = false; 
} 
else if ($_SESSION['debug'] == true || isset($_GET['debug'])) {
    $_SESSION['debug'] = true; 
    include('debuglog.php'); 
} 
?>