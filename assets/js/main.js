var modalData = null;
var modalTrigger = null;

var selectedTabName = 'vyroba';

var mainPageTabs = [
    {
        tabName: 'restaurace',
        description: 'Restaurace, bary, kavárny, …',
        listItems: [
            'zajištění bezpečnosti a ochrany zdraví při práci',
            'zajištění požární ochrany',
            'pravidelné kontroly a audity BOZP',
            'pravidelné vyhledávání a vyhodnocování rizikových faktorů',
            'zajištění prevence a následných odstranění rizik',
            'kategorizace prací dle stupně rizika',
            'kontroly a revize všech technických zařízení',
            'ergonomie práce a pracovního prostředí',
            'zpracování směrnic OOPP (osobní ochranné pracovní pomůcky)',
            'evidence a hlášení pracovních úrazů',
            'školení zaměstnanců v oblasti BOZP',
            'a další povinnosti podle typu podniku'
        ]
    },
    {
        tabName: 'hotel',
        description: 'Hotely, ubytovací zařízení, …',
        listItems: [
            'zajištění bezpečnosti a ochrany zdraví při práci',
            'zajištění požární ochrany',
            'pravidelné kontroly a audity BOZP',
            'pravidelné vyhledávání a vyhodnocování rizikových faktorů',
            'zajištění prevence a následných náprav rizik',
            'kategorizace prací dle výše rizika',
            'kontroly a revize všech technických zařízení',
            'ergonomie práce a pracovního prostředí',
            'zpracování směrnic OOPP (osobní ochranné pracovní pomůcky)',
            'evidence a hlášení pracovních úrazů',
            'školení zaměstnanců v oblasti BOZP',
            'a další povinnosti podle typu podniku'
        ]
    },
    {
        tabName: 'nemocnice',
        description: 'Zdravotnická zařízení, …',
        listItems: [
            'analýza a vyhodnocení rizik',
            'stanovení preventivních opatření rizik',
            'zpracování registru a řízení rizik',
            'kategorizace prací podle rizikovosti',
            'pravidelné prohlídky a audity BOZP',
            'zpracování provozní dokumentace BOZP',
            'pravidelná aktualizace dokumentace BOZP',
            'školení zaměstnanců v oblasti BOZP',
            'seznámení s právními předpisy',
            'evidence a hlášení pracovních úrazů',
            'ergonomie práce a pracovního prostředí',
            'zajištění osobních ochranných pracovních prostředků (OOPP)',
            'zajištění revizí a kontrol technických zařízení a lékařských přístrojů',
            'a další povinnosti podle typu podniku'
        ]
    },
    {
        tabName: 'vzdelani',
        description: 'Vzdělávací zařízení, školy, …',
        listItems: [
            'vyhledávání a prevence rizikových faktorů',
            'evidence a aktualizace dokumentace BOZP a PO',
            'výuka bezpečnosti a ochrany zdraví (BOZ)',
            'školení BOZP všech zaměstnanců školy',
            'školení práce s biologickými činiteli',
            'zpracování podkladů pro výuku první pomoci',
            'seznámení s právními předpisy',
            'evidence a hlášení pracovních úrazů',
            'ergonomie práce a pracovního prostředí',
            'zajištění osobní ochranných pracovních prostředků (OOPP)',
            'a další povinnosti podle typu podniku'
        ]
    },
    {
        tabName: 'stavba',
        description: 'Architekti, stavebnictví …',
        listItems: [
            'zpracování povinné dokumentace',
            'zpracování kategorizace prací',
            'vyhledávání potenciálně nebezpečných faktorů',
            'poskytnutí osobních ochranných pracovních prostředků (OOPP)',
            'předání podkladů pro koordinátora BOZP',
            'pravidelná aktualizace dokumentace BOZP',
            'prevence a zajištění nápravy proti rizikům',
            'školení zaměstnanců ve stavebnictví',
            'realizace revizí a kontrol technických zařízení',
            'rozmístění bezpečnostního značení na staveništi',
            'účast na provádění pravidelných kontrol a auditů BOZP',
            'a další povinnosti podle typu stavby'
        ]
    },
    {
        tabName: 'vyroba',
        description: 'Výroba, sklady, dílny …',
        listItems: [
            'průběžné vyhledávání a zhodnocení rizik',
            'seznámení všech zaměstnanců s riziky',
            'seznámení zaměstnanců s lokálními požadavky',
            'přijímání preventivních opatření vůči rizikům',
            'kategorizace jednotlivých pracovních činností',
            'vypracování a aktualizace dokumentace BOZP',
            'školení BOZP, PO, či specifická školení',
            'školení obsluhy strojů a zařízení',
            'evidence záznamů o pracovních úrazech',
            'zpracování traumatologického plánu',
            'revize a kontroly technických zařízení a strojů',
            'ergonomie práce a pracovního prostředí',
            'vypracování směrnic pro manipulaci a skladování',
            'zajištění pravidelných kontrol a auditů BOZP',
            'poskytnutí OOPP',
            'a další povinnosti podle typu firmy'
        ]
    },
    {
        tabName: 'obchod',
        description: 'Obchody, prodejny, sklady, …',
        listItems: [
            'zajištění a aktualizace dokumentace BOZP',
            'zajištění a aktualizace dokumentace PO',
            'průběžné vyhledávání a vyhodnocování rizik',
            'zajištění nápravných a preventivních opatření',
            'zpracování registru nalezených potenciálních rizik',
            'analýza a kategorizace prací dle stupně rizika',
            'zajištění pravidelných auditů BOZP a PO',
            'správa pracovních úrazů (evidování a hlášení)',
            'poskytnutí osobních ochranných pracovních pomůcek (OOPP)',
            'pravidelné kontroly a revize všech strojí a zařízení',
            'zpracování směrnic a řádů pro sklady',
            'školení zaměstnanců dle profesí (prodejci, skladníci, řidiči, atd.)',
            'školení pracovníků - poskytnutí první pomoci',
            'zpracování traumatologického plánu',
            'zpracování a rozmístění bezpečnostních značek',
            'a další povinnosti podle typu provozu'
        ]
    }
];


function switchTabTo(selectedTabName) {

    var tabContainer = $('.tab-tabs-container .tab-container');
    var colListLeft = tabContainer.find('.row-infolists div[class^="col"]').first().find('ol');
    var colListRight = tabContainer.find('.row-infolists div[class^="col"]').last().find('ol');

    colListLeft.find('li').remove();
    colListRight.find('li').remove();
    
    var selectedTab = mainPageTabs.filter( function(tab) {
        return tab.tabName === selectedTabName;
    })[0];

    var tabDescriptionText = tabContainer.find('.row').first().find('h2 strong');
    tabDescriptionText.text(selectedTab.description);

    selectedTab.listItems.forEach( function(item, itemIndex, itemsArray) {
        if (itemIndex < Math.round(itemsArray.length / 2)) {
            colListLeft.append($('<li></li>').text(item));
        }
        else {

            colListRight.append($('<li></li>').text(item));
        }
        colListRight.attr('start', Math.round(itemsArray.length / 2) + 1);
    });

};

$(document).ready(function () {


    if ($(".navbar-nav").is(":visible")) {
        $('.navbar-nav .nav-item').each(function () {
            $(this).width($(this).width());
        });
    }

    $(window).resize(function () {
        if ($(".navbar-nav").is(":visible")) {
            $('#navbarsExampleContainer').collapse('hide');
            setTimeout(function() {
                $('.navbar-nav .nav-item').each(function () {
                    $(this).width($(this).width());
                    $(this).removeClass('wide-full');
                });
            }, 500)
        }
        else {
            $('.navbar-nav .nav-item').each(function () {
                $(this).addClass('wide-full');
            });
        }
    });

    switchTabTo('vyroba');

    $('#tab-toggle-vyroba').addClass('active');
    $('#tab-vyroba').addClass('active');    

    var slideshowTimer = startSlideshow(3500);
    
    function startSlideshow(interval) {
        return setInterval( function() { switchNextSlide(); }, interval);
    }

    function stopSlideshow(slideshowTimerInterval) {
        clearInterval(slideshowTimerInterval);
        return null;
    }

    function onClickAction() {
        $('.tab-toggle').unbind('click');
        if (!slideshowTimer && $(this).hasClass('active')) {
            slideshowTimer = startSlideshow(3500);
        }
        else {
            slideshowTimer = stopSlideshow(slideshowTimer);
            selectSlide(this);
        }
        $('.tab-toggle').click(onClickAction);
    }
    
    $('.tab-toggle').click(onClickAction);

    function selectSlide(slideToggle) {
        $('.tab-toggle').removeClass('active');
        $(slideToggle).addClass('active');
        var tabIdSelector = '#' + slideToggle.id.replace('-toggle', '');
        selectedTabName = slideToggle.id.replace('tab-toggle-', '');
        switchTabTo(selectedTabName);
    }

    function switchNextSlide() {
        var nextToggle = $('.tab-toggle.active').next()[0] || $('.imgnavbar .tab-toggle').first()[0];
        if (!nextToggle) {
            return;
        }
        $('.tab-toggle.active').removeClass('active');
        $(nextToggle).addClass('active');
        var fadeOutContent = $('.tab-container span, .tab-container li');
        fadeOutContent.fadeOut(225, function() {
            selectedTabName = nextToggle.id.replace('tab-toggle-', '');
            switchTabTo(selectedTabName);
            fadeOutContent.fadeIn(225);
        });
    }

    $('#addCompanyForm input[name="subjectTypeRadios"]').change(function () { 
        $('#addCompanyForm select')[0].disabled = this['value'] === 'addCompany'; 
        if ($('#addCompanyForm select')[0].disabled) { 
            $('#addCompanyForm select')[0].options.selectedIndex = 0; 
        } 
    });

    $('#companyDeleteButtonContainer button').click(function(event) {
        $('#companyDeleteButtonContainer').hide();
        $('#companyDeleteConfirmationDialog').show();
    });

    $('#companyDeleteConfirmationDialog button').click(function(event) {
        if ($(this).val() === 'yes') {
            $.post( "/assets/php/ajax/deletecompany.php", 
                { 
                    companyId: $('#companyDeleteButtonContainer button').val()
                }, 
                function(data, status){ 
                    if (data === "success") { 
                        location.reload(); 
                        alert('Společnost byla úspěšně smazána'); 
                    } 
                    else { 
                        alert(data); 
                    } 
                } 
            );
        }
        $('#companyDeleteConfirmationDialog').hide();
        $('#companyDeleteButtonContainer').show();
    }); 
        
    $('.company-button-add').click(function(event) { 
        var postObject = {}; 
        var parentCompanyId = $('#companyNameSelect').val(); 
        if (parentCompanyId) { 
            postObject['parentCompanyId'] = parentCompanyId; 
        } 
        $('.add-company-form input').each(function() { 
            postObject[this.name] = $(this).val(); 
        }); 
        $.post("/assets/php/ajax/addcompany.php", 
            postObject, 
            function(data, status) { 
                if (data === "success") { 
                    location.reload(); 
                    alert('Společnost byla úspěšně přidána.'); 
                } 
                else { 
                    alert(data); 
                } 
            } 
        ); 
    });
    
    $('.document-button-delete').click(function(event) { 
        documentId = $(this).val();
        $.post("/assets/php/ajax/deletedocument.php", 
            {
                documentId: documentId
            }, 
            function(data, status) { 
                if (data === "success") { 
                    location.reload(); 
                    alert('Dokument byl úspěšně smazán.'); 
                } 
                else { 
                    alert(data); 
                } 
            } 
        ); 
    });
    
    $('.table-documents td').click(makeInput);
    
    function makeInput() {
        var tableCell = this;
        var tableRow = $(tableCell).closest('tr');
        var tableRowCells = tableRow.find('td');
        var tableHeaders = tableRow.closest('table').find('th');
        if ($(tableHeaders[tableCell.cellIndex]).attr('editable') !== undefined) {
            var input = $('<input type="' + $(tableHeaders[tableCell.cellIndex]).attr('type') + '" value="' + ($(tableCell).find('span.hidden').text() || $(tableCell).text()) + '" />');
            if ($(tableCell).hasClass('td-date-null')) {
                $(input).val('');
            }
            var initialContent = $(tableCell).html();
            $(tableCell).html('');
            $(tableCell).append(input);
            $(input).focus();
            $('.table-documents td').unbind();
            $(tableCell).on('keyup', function(e) {
                if (e.keyCode === 13) {
                    var postObject = { documentId: tableRow.find('.document-button-delete').val() * 1 };
                    postObject[$(tableHeaders[tableCell.cellIndex]).attr('name')] = $(input).val();
                    $.post( "/assets/php/ajax/updatedocument.php", 
                        postObject,
                        function(data, status){ 
                            if (data === "success") { 
                                location.reload(); 
                                alert('Změny byly úspěšně uloženy.'); 
                            } 
                            else { 
                                alert(data); 
                            } 
                        } 
                    );
                }
                else if (e.keyCode === 27) {
                    $(tableCell).html(initialContent);
                    $('.table-documents td').click(makeInput);
                }
            });
        }
        else if ($(tableHeaders[tableCell.cellIndex]).attr('switchable') !== undefined) {
            $.post( "/assets/php/ajax/updatedocumentguard.php", 
                {
                    documentId: tableRow.find('.document-button-delete').val() * 1,
                    dateGuard: $(tableCell).text() === 'ANO' ? 0 : 1
                },
                function(data, status){ 
                    if (data === "success") { 
                        location.reload(); 
                        alert('Změny byly úspěšně uloženy.'); 
                    } 
                    else { 
                        alert(data); 
                    } 
                } 
            );
        }
    }
    
    $('#companyUpdateButton').click(function(event) {
        var postObject = {}; 
        postObject['companyId'] = $(this).val();
        $('.update-company-table input, .update-contact-person-table input').each(function() { 
            postObject[this.name] = $(this).val(); 
        }); 
        $.post( "/assets/php/ajax/updatecompany.php", 
            postObject,
            function(data, status){ 
                if (data === "success") { 
//                    location.reload(); 
                    alert('Změny byly úspěšně uloženy.'); 
                } 
                else { 
                    alert(data); 
                } 
            } 
        );
    });

    $(".resizable").colResizable({liveDrag:true});

    $(".table-companies tbody tr").mouseover( function(event) {
        $(this).find('td').css('background-color', 'lightgray');
        $(this).mouseout( function() {
            $(this).find('td').css('background-color', 'white');
        });
    });
    
    $(".table-companies tbody tr").click( function(event) {
        if (event.target.type === 'button') {
            return;
        }
        var names = [];
        var values = [];
        $(this).closest('table').find('th').each( function() {
            names.push($(this).text());
        });
        $(this).find('td').each( function() {
            if ($(this).children('.hidden').length === 1) {
                values.push($(this).children().not('.hidden')[0].innerHTML);
            }
            else {
                values.push($(this).children().length > 1 ? $(this).find('*:last-child').text() : $(this).text()); 
            }
        });
        var dataset = [];
        names.forEach( function(name, index) {
            if (values[index]) {
                dataset.push({
                    name: names[index],
                    value: values[index]
                });
            }
        });
        modalData = dataset;
        modalTrigger = this;
        $(modalTrigger).css('background-color', 'yellow');
        $("#myModal").modal({keyboard: true});
    });
    
    $("#myModal").on('show.bs.modal', function () {
        var modalTable = $('<table></table>');
        modalTable.css('margin', 'auto');
        modalData.forEach( function(row) {
            var modalTableRow = $('<tr><td>' + row.name + '</td><td>' + row.value + '</td></tr>');
            modalTable.append(modalTableRow);
        });
        $(this).find('.modal-body').append(modalTable);
    });
    
    $("#myModal").on('hidden.bs.modal', function () {
        $(this).find('table').remove();
        modalData = null;
        $(modalTrigger).css('background-color', 'white');
        modalTrigger = null;
    });

    $(".table-companies").tablesorter({ 
        sortList: [[4,0]],
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            16: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            // assign the third column (we start counting zero) 
            17: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            } 
        },
        // custom text extraction function 
        textExtraction: extractInfo
    });

    $(".table-documents").tablesorter({ 
        sortList: [[2,0]],
        headers: { 
            4: { 
                sorter: false 
            }
        },
        textExtraction: extractInfo
    });

    // define a custom text extraction function 
    function extractInfo(node) {
        var hiddenSubnodes = $(node).find('.hidden'); 
        if (hiddenSubnodes.length === 1) { 
            return hiddenSubnodes[0].innerHTML; 
        } 
        if (node.childNodes.length >= 2) {
            return node.childNodes[node.childNodes.length - 1].innerHTML;
        }
        else {
            return node.innerHTML;
        }
    }

	$('.form-user-info').submit(function (event) {
        if (this.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();    
        }
        else {
            $(event.target).closest('.container').addClass('hidden').next().removeClass('hidden');
            $('.proccess-nav-container span.active').removeClass('active');
            $('.proccess-nav-container span').last().addClass('active');
        
            $('.userinfo-col span').first().text($('input#name')[0].value + ' ' + $('input#surname')[0].value);
        
            $('.userinfo-col span').last().text($('select#company')[0].options[$('select#company')[0].options.selectedIndex].innerText);
            $('.userinfo-col').closest('.row').removeClass('text-center');
            $('.userinfo-col').closest('.row').addClass('text-left');
            $('.userinfo-col').removeClass('hidden');

            $('.question-row').first().removeClass('hidden');
        }
        $('.form-user-info').addClass('was-validated');
        return false;
    });

	$('.checkbox-required').change(function () {
		if ($('.checkbox-required').filter(function () {
			return !$(this).prop('checked');
		}).length === 0) {
            $('#startTestButton').attr('disabled', null);
            $('#startTestButton').removeClass('disabled');
		} else {
            $('#startTestButton').attr('disabled', true);
            $('#startTestButton').addClass('disabled');
		}
    });

    var clickInterval = null;

});

