<?php
    require_once ('../../../loader.php');
    if (isLogged('superuser')) {
        logout('superuser');
    }
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/adminlogin');
?>