<?php 
require_once('../../../loader.php');
$error = '';
if (isset($_POST['superusername']) && isset($_POST['superuserpassword'])) { 
    try { 
        login($_POST['superusername'], $_POST['superuserpassword'], 'superuser'); 
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/admin', true, 303); 
        exit; 
    } catch (Exception $e) { 
        $error = $e->getMessage(); 
    } 
} 
header('Location: http://' . $_SERVER['HTTP_HOST'] . '/adminlogin', true, 303); 
?>