
<?php
require_once('../../../loader.php');

if (!isLogged('companyMaster')) {
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/dokumenty');
}
if (isset($_GET['id'])) {

    require('../libs/tfpdf/tfpdf.php');

    $pdf = new tFPDF();
    $pdf->AddPage();
    
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVuBold','','DejaVuSansCondensed-Bold.ttf',true);
    $pdf->AddFont('DejaVuOblique','','DejaVuSansCondensed-Oblique.ttf',true);

    $pdf->SetFont('DejaVuBold','U',15);
    $pdf->Cell(0,30,'Test',0,2,'C');

    $resultTest = dibi::query('SELECT * FROM [Tests] WHERE testId=%i', $_GET['id']);
    $test = $resultTest->fetch();

    $pdf->SetFont('DejaVuBold','',9);
    $pdf->Cell(40,8,'Firma: ');
    $pdf->SetFont('DejaVu','',11);
    $company = getCompanyById($test['companyId']);
    $pdf->Cell(0,8,$company['name']);
    $pdf->Ln();

    $pdf->SetFont('DejaVuBold','',9);
    $pdf->Cell(40,8,'Typ testu: ');
    $pdf->SetFont('DejaVu','',11);
    $pdf->Cell(0,8,$test['name']);
    $pdf->Ln();

    $pdf->SetFont('DejaVuBold','',9);
    $pdf->Cell(40,8,'Vyplnil: ');
    $pdf->SetFont('DejaVu','',11);
    $pdf->Cell(0,8,$test['submittedBy']);
    $pdf->Ln();

    $pdf->SetFont('DejaVuBold','',9);
    $pdf->Cell(40,8,'Datum odevzdání: ');
    $pdf->SetFont('DejaVu','',11);
    $pdf->Cell(0,8,$test['testDate']->format("d. m. Y, H:i:s"));
    $pdf->Ln();

    $pdf->Ln();
    
    $pdf->SetFont('DejaVuBold','',9);
    $pdf->Cell(0,15,'Otázky:');
    $pdf->Ln();

    $resultQuestions = dibi::query('SELECT TestQuestions.questionId, Questions.question FROM [Tests] JOIN [TestQuestions] ON Tests.testId = TestQuestions.testId JOIN [Questions] ON TestQuestions.questionId = Questions.questionId WHERE Tests.testId=%i', $_GET['id']);
    $questions = $resultQuestions->fetchAll();
    foreach ($questions as $m => $question) {
        
        $pdf->SetFont('DejaVu','',9);
        $pdf->Cell(5);
        $pdf->Cell(5,5,$m+1 . '.  ');
        $pdf->MultiCell(0,5,$question['question']);
        $pdf->Ln(2);

        $resultAnswers = dibi::query('SELECT Questions.questionId, Answers.answerId, Answers.answer, Answers.isCorrect FROM [Questions] JOIN [Answers] ON Questions.questionId = Answers.questionId WHERE Questions.questionId=%i', $question['questionId']);
        $answers = $resultAnswers->fetchAll();
        foreach ($answers as $answer) {
            $pdf->Cell(12);
            if ($answer['isCorrect']) {
                $pdf->SetFont('DejaVuBold','',9);
                $pdf->Cell(3,4.5,'+');
            } else {
                $pdf->SetFont('DejaVu','',9);
                $pdf->Cell(3,4.5,'-');
            }
            $pdf->MultiCell(0,5,$answer['answer']);
            $pdf->Ln(2);
        }
        $pdf->Ln(7);
    }

    $pdf->Ln(14);
    $pdf->Cell(130);
    $pdf->SetFont('DejaVuBold','',9);
    $pdf->Cell(40,8,'Výsledek testu: ');
    $pdf->SetFont('DejaVu','',11);
    $pdf->Cell(0,8,'Uspěl');

    $pdf->Output();

} else {
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/dokumenty');
}
?>