<?php
require_once('../../../loader.php');
$error = '';
if (isset($_POST['companyUserPassword'])) {
    try {
        companyUserLogin($_POST['companyUserPassword']);
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/skoleni', true, 303);
        exit;
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
?>