<?php
require_once('../../../loader.php');
$error = '';
if (isset($_POST['companyMasterPassword'])) {
    try {
        companyMasterLogin($_POST['companyMasterPassword']);
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/dokumenty', true, 303);
        exit;
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
?>