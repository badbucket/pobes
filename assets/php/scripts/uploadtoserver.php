<?php
require_once('../../../loader.php'); 
$ok = 1;
try {
    $name = $_FILES['uploaded']['name'];
    $nameNorm = URLify::filter($name, 60, 'cs', $file_name = true);
    $type = $_POST['type'];
    $dateValid = $_POST['validdate'];
    $dateGuard = $_POST['dateguard'] === 'on' ? 1 : 0;
    $arr = array(
        'name' => $name,
        'nameNorm' => $nameNorm,
        'companyId' => $_GET['id'],
        'dateCreated' => new DateTime,
        'type' => $type,
        'dateValid' => $dateValid ? new DateTime($dateValid) : NULL,
        'dateGuard' => $dateGuard,
    );
    $insert = dibi::query('INSERT INTO [Documents]', $arr);
    $id = dibi::getInsertId();
    $target = '../../../data/uloziste' . '/' . $id . '_' . basename($nameNorm);
    if (move_uploaded_file($_FILES['uploaded']['tmp_name'], $target)) {
        $company = getCompanyById($_GET['id']);
        sendMail(
            $company['email'],
            'Nový dokument',
            '<p>Dobrý den,</p>
            <p>Na vaše datové úložiště jsme nahráli nový dokument.</p>
            <br>
            <p>Firma/pobočka: <strong>' . $company['name'] . '</strong></p>
            <p>kategorie: <strong>' . $type . '</strong></p>
            <p>název dokumentu: <strong>' . $name . '</strong></p>
            <p>datum platnosti do: <strong>' . date('d. m. Y', strtotime($dateValid)) . '</strong></p>
            <br>
            <p>Tento email je pouze informativní. Nemusíte se o nic starat.</p>
            <p>My se automaticky postaráme o to, aby byl váš dokument stále aktuální.</p>
            <br>
            <p>S přátelským pozdravem</p>
            <p><i>Váš tým společnosti POBES</i></p>
            <a href="http://www.pobes.cz/">www.pobes.cz</a>',
            'hlidac@pobes.cz'
        );
        echo "The file ". basename( $_FILES['uploaded']['name']). " has been uploaded";
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/admin?company=' . $_GET['id'] .'&document&success');
    }  
    else { 
        echo "Sorry, there was a problem uploading your file.";
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/admin?company=' . $_GET['id'] .'&document&fail');
    }
} catch (Exception $e) {
    $error = $e->getMessage();
    echo $error;
    exit;
}
?>