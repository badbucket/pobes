<?php

define("PROTOCOL", "http://");
define("DOMAIN", "www.pobes.cz");
define("ROOT", PROTOCOL . DOMAIN);
define("ASSETS",  ROOT . "/assets");
define("DATA", ROOT . "/data");
define("AUTHOR", "Jakub Skála");
define("TITLE", "POBES");
define("DEFAULT_PAGE", "pobes");
define("ERROR_PAGE", "pobes");
define("CONTACT_EMAIL", "blamoid@gmail.com");
define("CONTACT_PERSON", "Jakub Skála");

$urls = array("pobes", "bozp", "po", "dokumenty", "skoleni", "admin", "adminlogin");

if (isset($_GET['url'])) {
	$url = explode("/", cover($_GET['url']));
	$link = getTemplate($urls, $url, DEFAULT_PAGE, ERROR_PAGE);
} else {
	$link = DEFAULT_PAGE;
}

$titlePre = 'POBES - ';

?>
