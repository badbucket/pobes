<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php');
if (!isLogged('superuser')) exit('UNAUTHORIZED');
if(isset($_POST['documentId'])) {
    try {
        $magicQuotes = get_magic_quotes_gpc();
        foreach ($_POST as $key => $item) {
            $item = trim($item);
            if ($magicQuotes) {
                $item = stripslashes($item);
            }
        }
        $affectedRows = dibi::query('UPDATE [Documents] SET', $_POST , 'WHERE documentId = ?', $_POST['documentId']);
        if (!$affectedRows) {
            throw new Exception('Failed to update the document.');
        }
        echo 'success';
    } catch (Exception $e) {
        print_r($e);
    }
}