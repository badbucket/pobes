<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php'); 
if (!isset($_SESSION['companyUser'])) exit('UNAUTHORIZED');
try {
    require_once ('../../../loader.php');
    if (isset($_POST['testSubmittedBy']) && isset($_POST['test'])) {
        $arr = array(
            'submittedBy' => $_POST['testSubmittedBy'],
            'testDate' => new DateTime,
            'companyId' => $_POST['companyId'],
            'name' => $_POST['name']
        );
        $insert = dibi::query('INSERT INTO [Tests]', $arr);
        $id = dibi::getInsertId();
        foreach ($_POST['test'] as $question) {
            $arr = array(
                'testId' => $id,
                'questionId' => $question['questionId']
            );
            $insert = dibi::query('INSERT INTO [TestQuestions]', $arr);
        }
        $message = '
            <p>Dobrý den,</p>
            <p>Pracovník <strong>' . $_POST['testSubmittedBy'] . '</strong> právě úspěšně prošel školením <strong>' . strtoupper($_POST['name']). '</strong>.</p>
            <p>Test byl uložen na vaše datové úložiště na stránkách <a href="http://www.pobes.cz/">www.pobes.cz</a>.</p>
            <br>
            <p>S přátelským pozdravem</p>
            <p><i>Váš tým společnosti POBES</i></p>
            <a href="http://www.pobes.cz/">www.pobes.cz</a>
        ';
        if ( sendMail(getCompanyById($_POST['companyId'])['email'], 'Školení Dokončeno', $message, 'skoleni@pobes.cz') ) {
            echo 'SUCCESS';
        }
        else { 
            echo 'FAIL'; 
        }
    }
} catch (Exception $e) {
    $error = $e->getMessage();
    echo $error;
}
?>