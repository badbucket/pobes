<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php');
if (!isLogged('superuser')) exit('UNAUTHORIZED');
if(isset($_POST['documentId'])) {
    try {
        $_POST['documentId'] = trim($_POST['documentId']);
        if (get_magic_quotes_gpc()) {
            $_POST['documentId'] = stripslashes($_POST['documentId']);
        }
        if (deleteDocument(1 * $_POST['documentId'])) {
            echo "success";
        }
        else {
            echo "fail";
        }
    } catch (Exception $e) {
        $error = $e->getMessage();
        echo $error;
    }
}
