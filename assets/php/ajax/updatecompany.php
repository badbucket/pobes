<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php');
if (!isLogged('superuser')) exit('UNAUTHORIZED');
if(isset($_POST['companyId'])) {
    try {
        $magicQuotes = get_magic_quotes_gpc();
        foreach ($_POST as $key => $item) {
            $item = trim($item);
            if ($magicQuotes) {
                $item = stripslashes($item);
            }
        }
        updateCompany($_POST['companyId'], $_POST['name'], $_POST['description'], $_POST['city'], $_POST['street'], $_POST['houseNumber'], $_POST['serialNumber'], $_POST['ico'], $_POST['email'], $_POST['phoneNumber'], $_POST['lastCheckupDate'] ? $_POST['lastCheckupDate'] : NULL, $_POST['nextCheckupDate'] ? $_POST['nextCheckupDate'] : NULL, $_POST['masterAccessCode'], $_POST['accessCode'], $_POST['contactName']);
        echo 'success';
    }
    catch (Exception $e) {
        $error = $e->getMessage();
        echo 'fail';
    }
}