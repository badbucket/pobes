<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php');
if (!isLogged('superuser')) exit('UNAUTHORIZED');
try {
    $_POST['name'] = trim($_POST['name']);
    $_POST['description'] = trim($_POST['description']);   
    $_POST['city'] = trim($_POST['city']); 
    $_POST['street'] = trim($_POST['street']);
    $_POST['houseNumber'] = trim($_POST['houseNumber']);
    $_POST['serialNumber'] = trim($_POST['serialNumber']);
    $_POST['phoneNumber'] = trim($_POST['phoneNumber']); 
    $_POST['email'] = trim($_POST['email']);
    $_POST['lastCheckupDate'] = $_POST['lastCheckupDate'] ? trim($_POST['lastCheckupDate']) : NULL;
    $_POST['nextCheckupDate'] = $_POST['nextCheckupDate'] ? trim($_POST['nextCheckupDate']) : NULL;
    $_POST['masterAccessCode'] = trim($_POST['masterAccessCode']);
    $_POST['accessCode'] = trim($_POST['accessCode']);
    if (get_magic_quotes_gpc()) {
        $_POST['name'] = stripslashes($_POST['name']);
        $_POST['description'] = stripslashes($_POST['description']);
        $_POST['city'] = stripslashes($_POST['city']);
        $_POST['street'] = stripslashes($_POST['street']);
        $_POST['houseNumber'] = stripslashes($_POST['houseNumber']);
        $_POST['serialNumber'] = stripslashes($_POST['serialNumber']);
        $_POST['phoneNumber'] = stripslashes($_POST['phoneNumber']); 
        $_POST['email'] = stripslashes($_POST['email']);
        $_POST['lastCheckupDate'] = stripslashes($_POST['lastCheckupDate']);
        $_POST['nextCheckupDate'] = stripslashes($_POST['nextCheckupDate']);
        $_POST['masterAccessCode'] = stripslashes($_POST['masterAccessCode']); 
        $_POST['accessCode'] = stripslashes($_POST['accessCode']);
    }
    if (isset($_POST['parentCompanyId'])) {
        addCompany($_POST['name'], $_POST['description'], $_POST['city'], $_POST['street'], $_POST['houseNumber'], $_POST['serialNumber'], $_POST['ico'], $_POST['email'], $_POST['phoneNumber'], $_POST['lastCheckupDate'], $_POST['nextCheckupDate'], $_POST['masterAccessCode'], $_POST['accessCode'], $_POST['parentCompanyId']);
    } else {
        addCompany($_POST['name'], $_POST['description'], $_POST['city'], $_POST['street'], $_POST['houseNumber'], $_POST['serialNumber'], $_POST['ico'], $_POST['email'], $_POST['phoneNumber'], $_POST['lastCheckupDate'], $_POST['nextCheckupDate'], $_POST['masterAccessCode'], $_POST['accessCode'], NULL);
    }
    echo 'success';
} catch (Exception $e) {
    $error = $e->getMessage();
    echo $error;
}