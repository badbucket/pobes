<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php');
if (!isLogged('superuser')) exit('UNAUTHORIZED');
if(isset($_POST['companyId'])) {
    try {
        $_POST['companyId'] = trim($_POST['companyId']);
        if (get_magic_quotes_gpc()) {
            $_POST['companyId'] = stripslashes($_POST['companyId']);
        }
        if (deleteCompany(1 * $_POST['companyId'])) {
            echo "success";
        }
        else {
            echo "fail";
        }
    } catch (Exception $e) {
        $error = $e->getMessage();
        echo $error;
    }
}