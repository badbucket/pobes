<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit('BADREQ');
require_once('../../../loader.php');
if (!isLogged('superuser')) exit('UNAUTHORIZED');
if(isset($_POST['documentId'])) {
    try {
        $_POST['documentId'] = trim($_POST['documentId']);
        $_POST['dateValid'] = trim($_POST['dateValid']);
        $_POST['dateGuard'] = trim($_POST['dateGuard']);
        if (get_magic_quotes_gpc()) {
            $_POST['documentId'] = stripslashes($_POST['documentId']);
            $_POST['dateValid'] = stripslashes($_POST['dateValid']);
            $_POST['dateGuard'] = stripslashes($_POST['dateGuard']);
        }
        $arr = array(
            'dateGuard' => $_POST['dateGuard']
        );
        $affectedRows = dibi::query('UPDATE [Documents] SET', $arr , 'WHERE documentId = ?', $_POST['documentId']);
        if (!$affectedRows) {
            throw new Exception('Failed to update the document.');
        }
        echo 'success';
    } catch (Exception $e) {
        print_r($e);
    }
}
