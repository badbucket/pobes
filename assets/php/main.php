<?php

    define('TIMEOUT', 3600);

    function addCompany($name, $description, $city, $street, $houseNumber, $serialNumber, $ico, $email, $phoneNumber, $lastCheckupDate, $nextCheckupDate, $masterAccessCode, $accessCode, $parentCompanyId) {
        $arr = array(
            'name' => $name,
            'description' => $description,
            'city' => $city,
            'street' => $street,
            'houseNumber' => $houseNumber,
            'serialNumber'  => $serialNumber,
            'ico' => $ico,
            'email'  => $email,
            'phoneNumber'  => $phoneNumber,
            'lastCheckupDate'  => $lastCheckupDate,
            'nextCheckupDate'  => $nextCheckupDate,
            'masterAccessCode'  => strlen($masterAccessCode) > 0 ? $masterAccessCode : NULL,
            'accessCode'  => strlen($accessCode) > 0 ? $accessCode : NULL,
            'timeCreated' => new DateTime
        );
        if (!is_null($parentCompanyId)) {
            $arr['parentCompanyId'] = 1 * $parentCompanyId;
        }
        $insert = dibi::query('INSERT INTO [Companies]', $arr);
        if (!$insert) {
            throw new Exception('Failed to save the company to the database.');
        }
    }
    
    function updateCompany($companyId, $name, $description, $city, $street, $houseNumber, $serialNumber, $ico, $email, $phoneNumber, $lastCheckupDate, $nextCheckupDate, $masterAccessCode, $accessCode, $contactName) {
        $arr = array(
            'name' => $name,
            'description' => $description,
            'city' => $city,
            'street' => $street,
            'houseNumber' => $houseNumber,
            'serialNumber'  => $serialNumber,
            'ico' => $ico,
            'email'  => $email,
            'phoneNumber'  => $phoneNumber,
            'lastCheckupDate'  => $lastCheckupDate,
            'nextCheckupDate'  => $nextCheckupDate,
            'masterAccessCode'  => strlen($masterAccessCode) > 0 ? $masterAccessCode : NULL,
            'accessCode'  => strlen($accessCode) > 0 ? $accessCode : NULL,
            'contactName' => $contactName
        );
        $affectedRows = dibi::query('UPDATE [Companies] SET', $arr , 'WHERE companyId = ?', $companyId);
        if (!$affectedRows) {
            throw new Exception('Failed to update the company.');
        }
    }

    function getAllCompanies() {
        $result = dibi::query('SELECT * FROM [Companies]');
        return $result;
    }

    function getParentCompanies() {
        $result = dibi::query('SELECT * FROM [Companies] WHERE parentCompanyId IS NULL');
        return $result;
    }

    function getSubCompanies($parentCompanyId) {
        $result = dibi::query('SELECT * FROM [Companies] WHERE parentCompanyId=%i', $parentCompanyId);
        $record = $result->fetchAll();
        return $record;
    }

    function getCompanyNames() {
        $result = dibi::query('SELECT companyId, name FROM [Companies]');
        $record = $result->fetchPairs('companyId', 'name');
        return $record;
    }

    function getParentCompanyNames() {
        $result = dibi::query('SELECT companyId, name FROM [Companies] WHERE parentCompanyId IS NULL');
        $record = $result->fetchPairs('companyId', 'name');
        return $record;
    }

    function getCompanyById($companyId) {
        $result = dibi::query('SELECT * FROM [Companies] WHERE companyId=%i', $companyId);
        $record = $result->fetch();
        return $record;
    }
    
    function getNumberOfCompanies() {
        return 1 * count(dibi::query('SELECT companyId FROM [Companies]'));
    }

    function getNumberOfSubcompanies($parentCompanyId) {
        return 1 * count(dibi::query('SELECT companyId FROM [Companies] WHERE parentCompanyId=%i', $parentCompanyId));
    }
    
    function hasSubcompanies($parentCompanyId) {
        return getNumberOfSubcompanies($parentCompanyId) >= 1;
    }
    
    function companyUserLogin($accessCode) {
        $result = dibi::query('SELECT companyId FROM [Companies] WHERE accessCode=%s', $accessCode);
        $record = $result->fetch();
        if ($record['companyId'] !== NULL) {
            $_SESSION['companyUser'] = $record['companyId'];
            $arr = array(
                'companyId' => $_SESSION['companyUser'],
                'operationType' => 'loginuser',
                'time'  => date('Y-m-d H:i:s', $_SESSION['time'])
            );
            dibi::query('INSERT INTO [CompanyOperations]', $arr);
        }
    }
    
    function companyMasterLogin($masterAccessCode) {
        $result = dibi::query('SELECT companyId FROM [Companies] WHERE masterAccessCode=%s', $masterAccessCode);
        $record = $result->fetch();
        if ($record['companyId'] !== NULL) {
            $_SESSION['companyMaster'] = $record['companyId'];
            $arr = array(
                'companyId' => $_SESSION['companyMaster'],
                'operationType' => 'loginmaster',
                'time'  => date('Y-m-d H:i:s', time())
            );
            dibi::query('INSERT INTO [CompanyOperations]', $arr);
        }
    }
    
    function companyLogout($accessMode) {
        $companyId = $_SESSION[$accessMode];
        unset($_SESSION[$accessMode]);
        $arr = array(
            'companyId' => $companyId,
            'operationType' => $accessMode == 'companyMaster' ? 'logoutmaster' : 'logoutuser',
            'time' => date('Y-m-d H:i:s', time())
        );
        dibi::query('INSERT INTO [CompanyOperations]', $arr);
    }

    function login($accountname, $password, $accountType) {
        $accountname = trim($accountname);
        $accountType = trim($accountType);
        $password = trim($password);
        logout($accountType);
        $result = dibi::query('SELECT username FROM [Persons] WHERE username=%s', $accountname);
        $rows = count($result);
        if ($rows == 0) {
            throw new Exception('Zadané uživatelské jméno je neplatné.');
        }
        if ($rows > 1) {
            throw new Exception('DATABÁZOVÁ CHYBA - Nalezeno více záznamů se zadaným uživatelským jménem.');
        }
        $result = dibi::query('SELECT * FROM [Accounts] WHERE accountType=%s AND accountname=%s AND password=%s', $accountType, $accountname, $password);
        $rows = count($result);
        if ($rows == 0) {
            throw new Exception('Zadaný přístupový účet nebo heslo je neplatné.');
        }
        if ($rows > 1) {
            throw new Exception('DATABÁZOVÁ CHYBA - Nalezeno více záznamů se zadaným přístupovým účtem.');
        }
        $record = $result->fetch();
        $_SESSION[$accountType] = $accountname;
        $_SESSION['time'] = time();
        $arr = array(
            'accountname' => $_SESSION[$accountType],
            'operationType' => 'login',
            'time'  => date('Y-m-d H:i:s', $_SESSION['time'])
        );
        dibi::query('INSERT INTO [AccountOperations]', $arr);
    }

    function isLogged($accountType) {
        return !empty($_SESSION[$accountType]);
    }

    function authenticate() {
        if (isLogged('superuser')) {
            if ($_SESSION['time'] < (time() - TIMEOUT / 2)) {
                logout('superuser');
            } else {
                $_SESSION['time'] = time();
            }
        }
    }

    function logout($accountType) {
        if (isset($_SESSION[$accountType])) {
            $accountname = $_SESSION[$accountType];
            unset($_SESSION[$accountType]);
            $arr = array(
                'accountname' => $accountname,
                'operationType' => 'logout',
                'time'  => date('Y-m-d H:i:s', time())
            );
            dibi::query('INSERT INTO [AccountOperations]', $arr);
        }
    }

    function deleteCompany($companyId) {
        if (hasSubcompanies($companyId)) {
            throw new Exception('Společnost nemohla být smazána, neboť pod ní patří jiné společnosti!'); 
        }
        if (hasTests($companyId)) {
            throw new Exception('Společnost nemohla být smazána, neboť pod jsou u ní evidovány testy zaměstnanců!'); 
        }
        else {
            $result = dibi::query('DELETE FROM [Companies] WHERE companyId=%i', $companyId);
            if (!$result) {
                throw new Exception('The company could not be deleted.');
            }
            return true;
        }    
    }
    
    function getDocument($documentId) {
        $result = dibi::query('SELECT documentId, name, nameNorm, dateCreated, dateValid, dateGuard FROM [Documents] WHERE documentId=%i', $documentId);
        $record = $result->fetch();
        return $record;
    }

    function deleteDocument($documentId) {
        $result = dibi::query('DELETE FROM [Documents] WHERE documentId=%i', $documentId);
        if (!$result) {
            throw new Exception('The document could not be deleted.');
        }
        return true;
    }
    
    function saveTest($submittedBy, $test) {
        $arr = array(
            'submittedBy' => $submittedBy,
        );
        $insert = dibi::query('INSERT INTO [Tests]', $arr);
        if (!$insert) {
            throw new Exception('Failed to save the test to the database.');
        }
        return $insert;
    }
    
    function getNumberOfTests($companyId) {
        return 1 * count(dibi::query('SELECT testId FROM [Tests] WHERE companyId=%i', $companyId));
    }

    function getNumberOfTestsInYear($companyId, $year) {
        return 1 * count(dibi::query('SELECT testId FROM [Tests] WHERE companyId=%i AND YEAR(testDate)=%i', $companyId, $year));
    }
    
    function hasTests($companyId) {
        return getNumberOfTests($companyId) >= 1;
    }

    function getQuestionGroup($questionGroupShortcut) {
        $result = dibi::query('SELECT * FROM [QuestionGroups] WHERE shortcut=%s', $questionGroupShortcut);
        return $result->fetch();
    }
    
    function makeTest($questionGroupShortcut) {
        $resultQuestions = dibi::query('SELECT * FROM [QuestionGroups] JOIN [Questions] ON QuestionGroups.questionGroupId = Questions.questionGroupId WHERE QuestionGroups.shortcut=%s', $questionGroupShortcut);
        $questions = $resultQuestions->fetchAll();
        foreach ($questions as $question) {
            $resultQuestionAnswers = dibi::query('SELECT * FROM Answers WHERE QuestionId=%i', $question['questionId']);
            $question->answers = $resultQuestionAnswers->fetchAll();
        }
        shuffle($questions);
        return array_slice($questions,0,10);
    }

    function cover($str){
        return htmlspecialchars($str);
    }

    function getTemplate($urls, $url, $default, $error) {
        if (isset($url[0])) {
            $key = array_search($url[0], $urls);
            if ($key != NULL) {
                $link = $urls[$key];
            } else {
                return $error;
            }
            if (isset($url[1])) {
                $key = array_search($url[1], $urls);
                if ($key != NULL) {
                    $link .= '/' . $urld[$key];
                } else {
                    return $error;
                }
            }
            return $link;
        }
        return $default;
    }
    
    function getCompanyDocuments($companyId, $includeSubcompanies) {
        if ($includeSubcompanies && hasSubcompanies($companyId)) {
            $result = dibi::query('SELECT * FROM [Companies] JOIN [Documents] ON Companies.companyId = Documents.companyId WHERE Companies.companyId = %i OR Companies.parentCompanyId = %i', $companyId, $companyId);
        }
        else {
            $result = dibi::query('SELECT * FROM [Documents] WHERE companyId=%i', $companyId);
        }
        $record = $result->fetchAll();
        return $record;
    }
    
    function getClosestDateDocument($companyId) {
         $result = dibi::query('SELECT dateValid FROM [Documents] WHERE companyId = %i AND dateGuard = 1 ORDER BY dateValid ASC LIMIT 1', $companyId);
         $record = $result->fetch();
         return $record;
    }

    function getGuardedDocuments() {
        $result = dibi::query('SELECT * FROM [Documents] WHERE dateGuard = 1 AND dateValid IS NOT NULL ORDER BY dateValid ASC');
        $record = $result->fetchAll();
        return $record;
    }   

    function sendMail($to, $subject, $message, $from) {
        $headers = "From: " . strip_tags(!is_null($from) ? $from : 'pobes@pobes.cz') . "\r\n";
        // $headers .= "BCC: novak.matyas@gmail.com, novak.wojtech@gmail.com, pobes@pobes.cz\r\n";
        $headers .= "BCC: skalaja@seznam.cz, blamoid@gmail.com, pobes@pobes.cz \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        $company = getCompanyById($doc['companyId']);
        $to = $company['email'];

        return mail($to, $subject, $message, $headers);
    }

?>
