<html class="no-js" lang="cs" dir="ltr">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	</head>
	<body>
	<?php
        require_once('../../../loader.php'); 

        unset($message);

        $documents = getGuardedDocuments();
        foreach ($documents as $m => $doc) { 
            if (date('Y-m-d', strtotime($doc['dateValid'])) == date('Y-m-d', strtotime('+1 month'))) {
                $company = getCompanyById($doc['companyId']);
                $to = $company['email'];
                $message = '
                    <p>Dobrý den,</p>
                    <p>dovolujeme si vás upozornit na blížící se termín ukončení platnosti dokumentu.</p>
                    <br>
                    <p>Firma/pobočka: <strong>' . $company['name'] . '</strong></p>
                    <p>kategorie: <strong>' . $doc['type'] . '</strong></p>
                    <p>název dokumentu: <strong>' . $doc['name'] . '</strong></p>
                    <p>datum platnosti do: <strong>' . date('d. m. Y', strtotime($doc['dateValid'])) . '</strong></p>
                    <br>
                    <p>Tento email je pouze informativní. Nemusíte se o nic starat.</p>
                    <p>O obnově dokumentu Vás budeme informovat.</p>
                    <br>
                    <p>S přátelským pozdravem</p>
                    <p><i>Váš tým společnosti POBES</i></p>
                    <a href="http://www.pobes.cz/">www.pobes.cz</a>
                ';
            }
            if (date('Y-m-d', strtotime($doc['dateValid'])) == date('Y-m-d', strtotime('now'))) {
                $company = getCompanyById($doc['companyId']);
                $to = $company['email'];
                $message = '
                    <p>Dobrý den,</p>
                    <p>dovolujeme si vás upozornit na dokument, jemuž dnes končí platnost.</p>
                    <br>
                    <p>Firma/pobočka: <strong>' . $company['name'] . '</strong></p>
                    <p>kategorie: <strong>' . $doc['type'] . '</strong></p>
                    <p>název dokumentu: <strong>' . $doc['name'] . '</strong></p>
                    <p>datum platnosti do: <strong style="color: red;">' . date('d. m. Y', strtotime($doc['dateValid'])) . '</strong></p>
                    <br>
                    <p>Tento email je pouze informativní. Nemusíte se o nic starat.</p>
                    <p>O obnově dokumentu Vás budeme informovat.</p>
                    <br>
                    <p>S přátelským pozdravem</p>
                    <p><i>Váš tým společnosti POBES</i></p>
                    <a href="http://www.pobes.cz/">www.pobes.cz</a>
                ';
            }
            if (isset($message)) {
                sendMail($to, 'Platnost dokumentu', $message, 'hlidac@pobes.cz');
                unset($message);
            }
        }

        unset($message);

        $companies = getAllCompanies();
        foreach ($companies as $m => $com) { 
            if (isset($com['nextCheckupDate'])) {
                $to = $com['email'];
                if (date('Y-m-d', strtotime($com['nextCheckupDate'])) == date('Y-m-d', strtotime('+1 month'))) {
                    $message = '
                        <p>Dobrý den,</p>
                        <p>dovolujeme si vás upozornit na blížící se termín pravidelného auditu.</p>
                        <br>
                        <p>Firma/pobočka: <strong>' . $com['name'] . '</strong></p>
                        <p>datum posledního auditu: <strong>' . date('d. m. Y', strtotime($com['nextCheckupDate'])) . '</strong></p>
                        <br>
                        <p>Tento email je pouze informativní. Nemusíte se o nic starat.</p>
                        <p><strong>V nejbližší době vás kontaktujeme a domluvíme termín auditu.</strong></p>
                        <br>
                        <p>S přátelským pozdravem</p>
                        <p><i>Váš tým společnosti POBES</i></p>
                        <a href="http://www.pobes.cz/">www.pobes.cz</a>
                    ';
                }
                else if (date('Y-m-d', strtotime($com['nextCheckupDate'])) == date('Y-m-d', strtotime('now'))) {
                    $message = '
                        <p>Dobrý den,</p>
                        <p>dovolujeme si vás upozornit na dnešní termín pravidelného auditu.</p>
                        <br>
                        <p>Firma/pobočka: <strong>' . $com['name'] . '</strong></p>
                        <p>datum posledního auditu: <strong>' . date('d. m. Y', strtotime($com['nextCheckupDate'])) . '</strong></p>
                        <br>
                        <p>Tento email je pouze informativní. Nemusíte se o nic starat.</p>
                        <p><strong>V nejbližší době vás kontaktujeme a domluvíme termín auditu.</strong></p>
                        <br>
                        <p>S přátelským pozdravem</p>
                        <p><i>Váš tým společnosti POBES</i></p>
                        <a href="http://www.pobes.cz/">www.pobes.cz</a>
                    ';
                }
                if (isset($message)) {
                    sendMail($to, 'Termín Auditu', $message, 'hlidac@pobes.cz');
                    unset($message);
                }
            }
        }
	?>
	</body>
</html>
