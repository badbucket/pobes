<?php
if (!isLogged('superuser')) {
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/adminlogin', true, 303);
}
$title = 'Administrátorský přístup';
$noFooter = TRUE;
?>