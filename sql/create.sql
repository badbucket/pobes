SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `Accounts` (
  `accountname` varchar(100) COLLATE cp1250_czech_cs NOT NULL,
  `password` varchar(255) COLLATE cp1250_czech_cs NOT NULL,
  `accountType` enum('superuser','user','company') COLLATE cp1250_czech_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `Answers` (
  `answerId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `name` text COLLATE cp1250_czech_cs,
  `answer` text COLLATE cp1250_czech_cs,
  `isCorrect` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `Companies` (
  `companyId` int(11) NOT NULL,
  `parentCompanyId` int(11) DEFAULT NULL,
  `accessCode` varchar(255) COLLATE cp1250_czech_cs DEFAULT NULL,
  `masterAccessCode` varchar(255) COLLATE cp1250_czech_cs DEFAULT NULL,
  `name` text COLLATE cp1250_czech_cs,
  `description` text COLLATE cp1250_czech_cs,
  `city` text COLLATE cp1250_czech_cs,
  `street` text COLLATE cp1250_czech_cs,
  `houseNumber` text COLLATE cp1250_czech_cs,
  `serialNumber` text COLLATE cp1250_czech_cs,
  `ico` varchar(30) COLLATE cp1250_czech_cs DEFAULT NULL,
  `email` text COLLATE cp1250_czech_cs,
  `phoneNumber` text COLLATE cp1250_czech_cs,
  `lastCheckupDate` date DEFAULT NULL,
  `nextCheckupDate` date DEFAULT NULL,
  `timeCreated` datetime DEFAULT NULL,
  `contactName` text COLLATE cp1250_czech_cs
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `Documents` (
  `documentId` int(11) NOT NULL,
  `companyId` int(11) DEFAULT NULL,
  `name` varchar(300) COLLATE latin2_czech_cs DEFAULT NULL,
  `nameNorm` varchar(300) COLLATE latin2_czech_cs DEFAULT NULL,
  `type` varchar(30) COLLATE latin2_czech_cs DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `path` varchar(500) COLLATE latin2_czech_cs DEFAULT NULL,
  `dateCreated` date DEFAULT NULL,
  `dateValid` date DEFAULT NULL,
  `dateGuard` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

CREATE TABLE IF NOT EXISTS `Persons` (
  `username` varchar(100) COLLATE cp1250_czech_cs NOT NULL,
  `name` text COLLATE cp1250_czech_cs,
  `email` text COLLATE cp1250_czech_cs,
  `phoneNumber` text COLLATE cp1250_czech_cs
) ENGINE=MyISAM DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `QuestionGroups` (
  `questionGroupId` int(11) NOT NULL,
  `shortcut` text COLLATE cp1250_czech_cs,
  `name` text COLLATE cp1250_czech_cs,
  `description` text COLLATE cp1250_czech_cs,
  `note` text COLLATE cp1250_czech_cs
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `Questions` (
  `questionId` int(11) NOT NULL,
  `questionGroupId` int(11) DEFAULT NULL,
  `name` text COLLATE cp1250_czech_cs,
  `studyText` text COLLATE cp1250_czech_cs,
  `question` text COLLATE cp1250_czech_cs
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `TestQuestions` (
  `testId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

CREATE TABLE IF NOT EXISTS `Tests` (
  `testId` int(11) NOT NULL,
  `companyId` int(11) DEFAULT NULL,
  `name` text COLLATE cp1250_czech_cs,
  `submittedBy` text COLLATE cp1250_czech_cs,
  `testDate` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=cp1250 COLLATE=cp1250_czech_cs;

ALTER TABLE `Accounts`
  ADD PRIMARY KEY (`accountname`);

ALTER TABLE `Answers`
  ADD PRIMARY KEY (`answerId`),
  ADD KEY `questionId` (`questionId`);

ALTER TABLE `Companies`
  ADD PRIMARY KEY (`companyId`),
  ADD UNIQUE KEY `accessCode` (`accessCode`),
  ADD UNIQUE KEY `masterAccessCode` (`masterAccessCode`),
  ADD KEY `parentCompanyId` (`parentCompanyId`),
  ADD KEY `companyId` (`companyId`);

ALTER TABLE `Documents`
  ADD PRIMARY KEY (`documentId`),
  ADD KEY `companyId` (`companyId`);

ALTER TABLE `Persons`
  ADD PRIMARY KEY (`username`);

ALTER TABLE `QuestionGroups`
  ADD PRIMARY KEY (`questionGroupId`);

ALTER TABLE `Questions`
  ADD PRIMARY KEY (`questionId`),
  ADD KEY `questionGroupId` (`questionGroupId`);

ALTER TABLE `TestQuestions`
  ADD PRIMARY KEY (`testId`,`questionId`),
  ADD KEY `testId` (`testId`),
  ADD KEY `questionId` (`questionId`);

ALTER TABLE `Tests`
  ADD PRIMARY KEY (`testId`),
  ADD KEY `companyId` (`companyId`);

ALTER TABLE `Answers`
  MODIFY `answerId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `Companies`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `Documents`
  MODIFY `documentId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `QuestionGroups`
  MODIFY `questionGroupId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `Questions`
  MODIFY `questionId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `Tests`
  MODIFY `testId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `Accounts`
  ADD CONSTRAINT `Accounts_Persons` FOREIGN KEY (`accountname`) REFERENCES `Persons` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Answers`
  ADD CONSTRAINT `Answers_Questions` FOREIGN KEY (`questionId`) REFERENCES `Questions` (`questionId`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Companies`
  ADD CONSTRAINT `Companies_parentCompany` FOREIGN KEY (`parentCompanyId`) REFERENCES `Companies` (`companyId`) ON UPDATE CASCADE;

ALTER TABLE `Documents`
  ADD CONSTRAINT `Documents_Companies` FOREIGN KEY (`companyId`) REFERENCES `Companies` (`companyId`) ON UPDATE CASCADE;

ALTER TABLE `Questions`
  ADD CONSTRAINT `Questions_QuestionGroups` FOREIGN KEY (`questionGroupId`) REFERENCES `QuestionGroups` (`questionGroupId`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `TestQuestions`
  ADD CONSTRAINT `TestQuestions_Tests` FOREIGN KEY (`testId`) REFERENCES `Tests` (`testId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TestQuestions_Questions` FOREIGN KEY (`questionId`) REFERENCES `Questions` (`questionId`) ON UPDATE CASCADE;

ALTER TABLE `Tests`
  ADD CONSTRAINT `Tests_Companies` FOREIGN KEY (`companyId`) REFERENCES `Companies` (`companyId`) ON UPDATE CASCADE;
