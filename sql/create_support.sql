CREATE TABLE IF NOT EXISTS `AccountOperations` (
  `accountOperationId` int(11) NOT NULL,
  `accountname` varchar(100) COLLATE latin2_czech_cs DEFAULT NULL,
  `operationType` enum('login','logout') COLLATE latin2_czech_cs DEFAULT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

CREATE TABLE IF NOT EXISTS `CompanyOperations` (
  `companyOperationsId` int(11) NOT NULL,
  `companyId` int(11) DEFAULT NULL,
  `operationType` enum('loginuser','logoutuser','loginmaster','logoutmaster') COLLATE latin2_czech_cs DEFAULT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

ALTER TABLE `AccountOperations`
  ADD PRIMARY KEY (`accountOperationId`),
  ADD KEY `accountname` (`accountname`);

ALTER TABLE `CompanyOperations`
  ADD PRIMARY KEY (`companyOperationsId`),
  ADD KEY `companyId` (`companyId`);

ALTER TABLE `AccountOperations`
  MODIFY `accountOperationId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `CompanyOperations`
  MODIFY `companyOperationsId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
