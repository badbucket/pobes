INSERT INTO `Persons` (`username`, `name`, `email`, `phoneNumber`) VALUES
('admin_pobes', 'Matyáš Novák', NULL, NULL),
('admin_blamoid', 'Jakub Skála', NULL, NULL);

INSERT INTO `Accounts` (`accountname`, `password`, `accountType`) VALUES
('admin_pobes', 'heslo', 'superuser'),
('admin_blamoid', 'returned211', 'superuser');

INSERT INTO `Companies` (`parentCompanyId`, `accessCode`, `masterAccessCode`, `name`, `description`, `city`, `street`, `houseNumber`, `serialNumber`, `ico`, `email`, `phoneNumber`, `lastCheckupDate`, `nextCheckupDate`, `timeCreated`, `contactName`) VALUES
(NULL, '111333', '7777777', 'Red castle hospitality group a. s.', 'Gastro provozovatele', 'Plzeň', 'Železniční', '3', '30100', NULL, 'panuska@gmail.com', '759 152 689', NULL, DATE_ADD(CURDATE(), INTERVAL -14 DAY), DATE_ADD(CURDATE(), INTERVAL -14 DAY), 'Panuška'),
(1, NULL, NULL, 'Pivnice 21', 'Restaurace', 'Plzeň', 'U Radbuzy', '15', NULL, NULL, 'pivnice21@gmail.com', NULL, NULL, NULL, DATE_ADD(CURDATE(), INTERVAL -13 DAY), NULL),
(1, NULL, NULL, 'U Dobráka', 'Restaurace', 'Praha', 'Stanislavova', '34', NULL, NULL, 'udobraka@gmail.com', NULL, NULL, NULL, DATE_ADD(CURDATE(), INTERVAL -12 DAY), NULL),
(1, NULL, NULL, 'Na Bílé Hoře', 'Restaurace', 'Plzeň', 'Bolevecká', '7', NULL, NULL, 'nbh@gamil.com', NULL, NULL, CURDATE(), DATE_ADD(CURDATE(), INTERVAL -11 DAY), NULL),
(NULL, 'ZS123', 'T123', 'TOP firma', 'děláme vše', 'Praha', 'Koterovská', '63', '6454684', NULL, 'topfirma@info.cz', '78954696', NULL, NULL, DATE_ADD(CURDATE(), INTERVAL -10 DAY), NULL),
(5, 'ZS1234', 'PT123', 'podTOP_1', 'děláme jen něco', 'Plzen', 'Stanicni', '63', NULL, NULL, NULL, NULL, NULL, DATE_ADD(CURDATE(), INTERVAL +1 DAY), DATE_ADD(CURDATE(), INTERVAL -9 DAY), NULL),
(NULL, '123456', 'Firma312', 'Testovací FIRMA', 'testovací popisek', 'Plzen', 'Stanicni', '63', '31200', NULL, 'novak.matyas@gmail.com', '737137384', NULL, DATE_ADD(CURDATE(), INTERVAL + 1 MONTH), DATE_ADD(CURDATE(), INTERVAL -8 DAY), 'Matyáš Novák'),
(7, '123456789', 'Firma789', 'FIRMA 1', 'podfirma', 'Praha', 'Polívkova', '32', '18500', NULL, NULL, NULL, NULL, DATE_ADD(CURDATE(), INTERVAL +2 MONTH), DATE_ADD(CURDATE(), INTERVAL -7 DAY), NULL),
(NULL, 'A951', 'A159', 'tadá', 'texty', 'Plkov', 'Hlavní třída', '58', '1235', NULL, 'pukavec@pukly.cz', '789456321', NULL, DATE_ADD(CURDATE(), INTERVAL +15 DAY), DATE_ADD(CURDATE(), INTERVAL -6 DAY), NULL);
