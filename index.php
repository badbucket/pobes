<?php
try {
    require_once('loader.php');
    if (file_exists("modules/".$link.".php")) {
        include("modules/".$link.".php");
    }
    include("templates/header.php");
    include("view/".$link.".php");
    if (!isset($noFooter)) {
        include("templates/footer.php");
    }
    include("templates/tail.php");
    include("templates/subtail.php");
} catch (Exception $e) {
    echo getError($e->getMessage());
    exit;
}
?>