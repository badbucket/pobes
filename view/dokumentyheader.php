<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row justify-content-center row-aligned-text">
                    <div class="col-8">
                        <h1 class="">
                            <strong><?php echo $company['name'] ?></strong><br>
                            <span><?php echo $subtitle; ?> dokumentů</span>
                        </h1>
                    </div>
                    <div class="col">
                        <form class="form-inline company-login-form" action="<?php echo ASSETS; ?>/php/scripts/dokumentylogout.php" method="post">
                            <a class="nav-link" href="<?php echo PROTOCOL.DOMAIN . '/dokumenty'; ?>"><button type="button" class="btn btn-primary btn-light-blue" name="company-master-back">ZPĚT</button></a>
                            <button type="submit" class="btn btn-primary btn-light-blue" name="company-master-logout-form-submit">odhlásit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row text-grey">
        <div class="col aligned-content-vert">
            <?php
                if (!isset($_GET['id'])) {
                    echo '<a href="' . PROTOCOL.DOMAIN . '/dokumenty/?id=' . $company['companyId'] . '" ';
                } else {
                    echo '<p ';
                }
                echo ' class="link-grey">';
            ?>
                <h3 class="company-name-header">
                    <strong><?php echo $company['name']; ?></strong>
                    <span><?php echo $company['city'] . ' ' . $company['serialNumber'] . ', ' . $company['street'] . ' ' . $company['houseNumber']; ?></span>
                </h3>
            <?php echo !isset($_GET['id']) ? '</a>' : '</p>'; ?>
        </div>
        <div class="col text-right">
            <?php
                if (!isset($_GET['id'])) {
                    echo 'Heslo pro školení: <span class="bold">' . $company["accessCode"] . '</span>';
                }
                else {
                    echo '<p class="text-left" style="padding-left: 15px; margin-top: 14px;">';
                        echo 'Kontakt: <strong>' . $company['contactName'] . ' / ' . $company['phoneNumber'] . ' / ' . $company['email'] . '</strong><br>';
                        if (is_object($company['nextCheckupDate'])) {
                            echo 'Pravidelný roční audit: <strong>' . $company["nextCheckupDate"]->format("d. m. Y") . '</strong> <i>(provést pravidelná online školní)</i><br>';
                        }
                    echo '</p>';
                    echo '<hr class="hr-black">';
                    echo '<p class="text-left" style="padding-left: 15px;">';
                        echo 'Dokumentace v roce: 2015 / 2016 / <strong class="text-green">2017</strong>';
                    echo '</p>';
                }
                ?>
        </div>
    </div>
</div>