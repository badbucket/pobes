<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row row-aligned-text">
                    <div class="col">
                        <h1 class="page-title-header">
                            <strong><?php echo $title; ?></strong> - <?php echo $subtitle; ?>
                        </h1>
                    </div>
                    <div class="col-5 notestyle no-mobile">
                        <p>
                            Neváhejte se zeptat. Rádi Vám s čímkoliv poradíme:<br>
                            <strong>pobes@pobes.cz / +420 606 640 304</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <ul class="list-items chapters">
                <li><a href="#collapse1-headline">Projektová činnost ve výstavbě</a></li>
                <li><a href="#collapse2-headline">Obecné informace</a></li>
                <li><a href="#collapse3-headline">Služby v požární ochraně</a></li>
                <li><a href="#collapse4-headline">Dokumentace PO</a></li>
                <li><a href="#collapse5-headline">Školení PO</a></li>
                <li><a href="#collapse6-headline">Kontroly a revize</a></li>
                <li><a href="#collapse7-headline">Odborné poradenství a konzultace</a></li>
                <li><a href="#collapse7-headline">Požární asistenční dohled</a></li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-6 grid-div">
            <img src="<?php echo ASSETS . '/img/po.jpg'; ?>" width="168" height="225" alt="PO - Požární ochrana">
        </div>
    </div>
</div>
<hr class="hr-black">
<div class="container">
    <div class="panel-group group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse1-headline" class="panel-title">
                    <a href="#collapse1-headline">Projektová činnost ve výstavbě</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">

                    <p>Vypracování požárně bezpečnostního řešení stavby dle vyhl. 246/2001 Sb. §41 jako součást projektové dokumentace pro všechny stupně stavebního řízení. Zpracování projektů všech druhů a rozsahů objektu.</p>
                    <p>Odborné koordinace spolupracujících profesí pro vytvoření funkčního projektového celku.</p>
                    <p>Zajištění inženýringu v oblasti PO a kompletního inženýringu ve spolupráci s partnerskými společnostmi.</p>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse2-headline" class="panel-title">
                    <a href="#collapse2-headline">Obecné informace</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Přehled obecných informací k PO</h5>
                    <p>Důležité je vyzdvihnout vztah podnikatelů k požární prevenci. Jak se podnikatelé dívají na preventivní požární ochranu? Na vyhodnocení stačí jedno slovo – různě. Lze najít zastánce názoru, že:</p>
                    <ul>
                        <li>Lépe je něco investovat do prevence a snížit tak rizika v podnikání, tj. snížit nebezpečí vzniku požáru,</li>
                        <li>nám se to nemůže stát, protože tímto způsobem a v takových podmínkách pracujeme dlouho a nic se nestalo,</li>
                        <li>rádi bychom věnovali této oblasti potřebnou pozornost, ale finanční situace podniku nám to neumožňuje,</li>
                        <li>výrobci potlačí informace o vlastnostech výrobku, které by mohly snížit jeho úspěch na trhu (třeba se na to nepřijde),</li>
                        <li>výrobci vybaví výrobek všemi potřebnými informacemi pro jeho správné i bezpečné užití atp..</li>
                    </ul>
                    <p>Když hoří byt, někdo ztrácí domov, něčí jistoty jsou v základu otřeseny. Když hoří v dílně, v kanceláři, ve skladu … něčí podnikání může být ohroženo. A to ještě není řeč o tom, že takový požár může ohrožovat nejen vlastního podnikatele (jeho zaměstnance, jeho majetek), ale též okolí (zdraví nebo životy lidí, případně majetek někoho jiného), nemluvě o možné kontaminaci prostředí. Jedná se o možné vážné následky, a proto je třeba upravit některé povinnosti zákony. Na čem se ale jistě shodneme všichni, je skutečnost, že nechceme být v žárném případě ohroženi někým jiným. Jedná-li se o nároky na jednání druhých, v tomto směru by měl každý zřejmě jasno.</p>
                    <p>V tomto směru si Vám dovolujeme nabídnout své služby jako osoby odborně způsobilé v požární ochraně. Nabízíme stálou a cenově zvýhodněnou spolupráci. Výkon osob odborně způsobilých v PO podle ustanovení § 11, zákona o PO.</p>
                    <p>Neváhejte a obraťte se na nás. Ochráníme Vaše podnikání a svou práci si obhájíme u kontrol.</p>
                    
                    <h5>Proč využívat externí firmu na zajištění požární ochrany?</h5>
                    <p>
                        <strong>Úspora nákladů</strong> – odborná firma zná veškeré náležitosti týkající se zajištění PO, má školené zaměstnance a praxi. Ušetří Vám čas potřebný na studium legislativních předpisů o PO.<br>
                        <strong>Jistota</strong> – externí odborná firma Vám zajistí předepsané pravidelné kontroly či školení. Provede veškerá potřebná opatření k omezování rizik vzniku požáru atd..<br>
                        <strong>Poradenství</strong> - rychle dosažitelné poradenství v oblasti požární ochrany prostřednictvím e-mailu nebo telefonu
                    </p>
                    <h5>Základní právní rámec oblasti PO:</h5>
                    <ul>
                        <li>Zákon č. 133/1985 Sb., o požární ochraně ve znění pozdějších předpisů,</li>
                        <li>Vyhláška č.246/2001 Sb., o požární prevenci,</li>
                        <li>Vyhláška č. 23/2008 Sb., o technických podmínkách požární ochrany staveb.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse3-headline" class="panel-title">
                    <a href="#collapse3-headline">Služby v požární ochraně</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Zajištění služeb PO obsahuje:</h5>
                    <ul>
                        <li>Provedení vstupní prohlídky (audit) pracovišť a objektů, tj. zjištění skutečného stavu zajištění PO,</li>
                        <li>návrh dalšího postupu při plnění zákonných povinností zaměstnavatele v oblasti PO,</li>
                        <li>předložení nabídky s návrhem termínů a ceny, která bude zpracována dle položek a oblastí.</li>
                    </ul>
                    <h5>Podrobnější přehled služeb požární ochrany:</h5>
                    <ul>
                        <li>Zajišťování činností osobou odborně způsobilou v požární ochraně (PO),</li>
                        <li>zpracování požadované dokumentace PO dle zákonných požadavků a vykonávaných činností zaměstnavatele,</li>
                        <li>trvalá spolupráce pro funkční zajišťování povinností v oblasti požární ochrany a to na základě dohody,</li>
                        <li>externí zajišťování plnění povinností v požární ochraně dle zákona o požární ochraně odborně způsobilou osobou v PO,</li>
                        <li>provádění pravidelných kontrol dodržování předpisů o požární ochraně - preventivních požárních prohlídek,</li>
                        <li>návrh náhradního organizačního, popřípadě technického opatření v případě, že je požárně bezpečnostní zařízení nezpůsobilé plnit ve stavbě/objektech svojí funkci,</li>
                        <li>zpracování a vedení dokumentace požární ochrany dle požadavků zákona o požární ochraně a vyhlášky o požární prevenci,</li>
                        <li>školení vedoucích zaměstnanců a zaměstnanců a další školení požární ochrany,</li>
                        <li>odborná příprava preventistů PO,</li>
                        <li>odborná příprava zaměstnanců (fyzických osob) zařazených do preventivních požárních hlídek,</li>
                        <li>provádění požárního asistenčního dohledu při činnostech se zvýšeným požárním nebezpečím,</li>
                        <li>vyhotovení písemného povolení k provádění jednorázových činností se zvýšeným požárním nebezpečím,</li>
                        <li>zajištění revize hydrantů a požárních vodovodů,</li>
                        <li>zajištění revize hasicích přístrojů,</li>
                        <li>provedení kontroly požárních ucpávek,</li>
                        <li>provedení požárně/bezpečnostního značení ve stavbě/objektech,</li>
                        <li>poskytování součinnosti při jednání s orgány Státního požárního dozoru - <strong>HZS</strong>.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse4-headline" class="panel-title">
                    <a href="#collapse4-headline">Dokumentace PO</a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Dokumentací požární ochrany se stanovují podmínky požární bezpečnosti provozovaných činností a prokazuje se plnění některých povinností stanovených předpisy o požární ochraně.</p>
                    <h5>Dokumentaci požární ochrany tvoří:</h5>
                    <ul>
                        <li>Požárně bezpečnostní řešení,</li>
                        <li>dokumentace o začlenění do kategorie podle míry požárního nebezpečí,</li>
                        <li>posouzení požárního nebezpečí,</li>
                        <li>stanovení organizace zabezpečení požární ochrany,</li>
                        <li>požární řád,</li>
                        <li>požární poplachové směrnice,</li>
                        <li>požární evakuační plán,</li>
                        <li>dokumentace zdolávání požáru,</li>
                        <li>řád ohlašovny požárů,</li>
                        <li>tematický plán a časový rozvrh školení zaměstnanců a odborné přípravy preventivních požárních hlídek a preventistů požární ochrany,</li>
                        <li>dokumentace o provedeném školení a odborné přípravě preventivních požárních hlídek a preventistů požární ochrany,</li>
                        <li>požární kniha,</li>
                        <li>další dokumentace obsahující podmínky požární bezpečnosti, zpracovávaná a schvalovaná, popř. vedená podle zvláštních předpisů.</li>
                    </ul>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/dokumentace_po.pdf'; ?>">Podrobnější informace k PO</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse5-headline" class="panel-title">
                    <a href="#collapse5-headline">Školení PO</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse in">
                <div class="panel-body">

                    <h5>Školení PO pro zaměstnance</h5>
                    <p>Školení zaměstnanců se provádí při nástupu do zaměstnání, při každé změně pracoviště nebo pracovního zařazení zaměstnance, pokud se tím mění i požární nebezpečí na pracovišti kam zaměstnanec přešel a dále se opakuje 1 x za 2 roky.</p>
                    <h5>Školení PO pro vedoucí zaměstnance</h5>
                    <p>Školení vedoucích zaměstnanců se provádí při nástupu do funkce, při každé změně pracoviště nebo pracovního zařazení zaměstnance, pokud se tím mění i požární nebezpečí na pracovišti kam zaměstnanec přešel a dále se opakuje 1 x za 3 roky.</p>
                    <h5>Odborná příprava preventistů PO</h5>
                    <p>Odborná příprava preventistů PO se provádí na základě zpracovaného tematického plánu a časového rozvrhu. Tematický plán a časový rozvrh školení vydává právní subjekt právnické osoby, u podnikající fyzické osoby tato osoba nebo jím pověřený zástupce.</p>
                    <h5>Odborná příprava členů preventivních požárních hlídek</h5>
                    <p>Odborná příprava se provádí před zahájením činnosti se zvýšeným nebo vysokým požárním nebezpečím a opakuje se 1x za rok. Odborná příprava zahrnuje teoretickou a praktickou část.</p>
                    <h5>Školení osob pověřených zabezpečením PO v mimopracovní době</h5>
                    <p>Školení zaměstnanců pověřených zabezpečováním požární ochrany v době sníženého provozu nebo v mimopracovní době se zajišťuje 1x za rok.</p>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/podrobne_informace_o_skoleni_po.pdf'; ?>">Podrobnější informace ke školení PO</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse6-headline" class="panel-title">
                    <a href="#collapse6-headline">Kontroly a revize</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse in">
                <div class="panel-body">

                    <h5>Přenosné hasicí přístroje</h5>
                    <p>Současná platná legislativa ČR požaduje, aby v rámci zajištění bezvadné funkce, byla na všech hasicích přístrojích v pravidelných intervalech prováděna minimálně jedenkrát ročně kontrola přímo u majitele hasicích přístrojů. Oprávnění kontroloři se pravidelně zúčastňují výcvikových kurzů pořádaných výrobcem hasicích přístrojů, kde získají průvodní dokumentaci s postupy provádění kontrol na kompletní sortiment hasicích přístrojů, které má výrobce ve svém výrobním programu.</p>
                    <h5>Požární ucpávky</h5>
                    <p>Požární ucpávky jsou považovány za požárně bezpečnostní zařízení pro omezení šíření požáru podle § 2 odst. (4) písm. f) vyhlášky č. 246/2001 Sb. – s ohledem na uvedený legislativní předpis je i v tomto případě nutné provádět pravidelné kontroly provozuschopnosti ve lhůtě 1 x ročně.</p>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/revize_hasiciho_pristroje.pdf'; ?>">Revize hasícího přístroje</a>
                        </p>
                    </div>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/kontrola_pozarnich_ucpavek.pdf'; ?>">Kontrola požárních ucpávek</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse7-headline" class="panel-title">
                    <a href="#collapse7-headline">Odborné poradenství a konzultace</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse in">
                <div class="panel-body">

                    <p>Oblast poradenství a odborných konzultací slouží pro potřeby podnikatelů, kteří se zajímají o to, jak předcházet požárům. Podnikatelské aktivity, a s tím související nároky na požární ochranu, jsou velmi rozmanité. Rozsah potřebných informací (znalostí) ke snížení nebezpečí vzniku požáru je tedy příliš rozsáhlý a v toto ohledu jsme připraveni nabídnout potřebné informace pro zvýšení požární bezpečnosti Vámi provozované činnosti a případně vás upozornit na další souvislosti.</p>
                    <p>K preventivní požární ochraně se vztahuje řada právních i technických předpisů, analýza nebezpečí může být zdařilá a určení preventivních opatření může být správné, jedná-li se o analýzu a opatření provedené odborně a ,,na míru“. Fakt že nebezpečí vzniku požáru je jedno z existujících podnikatelských rizik, které by mělo být součástí vyhodnocování rizik při podnikání, nám bohužel často připomínají požáry. Součástí poradenství, které by mělo sloužit podnikatelům k dobré rozvaze nad riziky, jsou dvě zásady, které by měly být dodržovány – poradce by měl upozorňovat na možné negativní situace, které mohou nastat a poučit podnikatele o tom, jak jim předcházet.</p>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse7-headline" class="panel-title">
                    <a href="#collapse7-headline">Požární asistenční dohled</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse in">
                <div class="panel-body">
                <p>Pořádáte společenskou akci a potřebujete zajištění požárního dozoru? Zajistíme vám dozor osobami s praxí a příslušnou odbornou způsobilostí.</p>
                <ul>
                    <li>Zajištění požárních hlídek při pořádání koncertů, společenských, sportovních aj. společenských akcích,</li>
                    <li>zajištění střežení objektu jako náhrada nefunkčnosti vybraných požárně bezpečnostních zařízení – např. při dokončování staveb.</li>
                    <li>Osoby zajišťující požární dohled působí na společenské akci jako velitel a členové preventivní požární hlídky,</li>
                    <li>na akci dorazí 2 hod. před jejím začátkem a v návaznosti na legislativní požadavky provedou nutná opatření vedoucí k jejímu bezpečnému zahájení,</li>
                    <li>v součinnosti s ostatními členy preventivní požární hlídky zajistí kontrolu plnění stanovených podmínek k zabezpečení požární ochrany před zahájením, v průběhu, při přerušení a po ukončení akce,</li>
                    <li>odchází cca 1 hod. po ukončení akce.</li>
                </ul>
                </div>
            </div>
        </div>
    </div> 
</div>