<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row"> 
                    <div class="col"> 
                    <h1>
                        <strong><?php echo $title; ?></strong> - <?php echo $subtitle; ?>
                    </h1>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div> 
<div class="container" style="min-height:750px;"> 
    <div class="row">
        <div class="col">
            <form class="form-admin-login" action="<?php echo ASSETS; ?>/php/scripts/adminlogin.php" method='post' accept-charset='UTF-8'> 
                <h3 class="instruction-text">Zadej vstupní údaje</h3> 
                <div class="form-group row"> 
                    <label for="superusername" class="col-sm-3 col-md-2 col-form-label">Jméno:</label> 
                    <div class="col-sm-9 col-md-10"> 
                        <input type="text" id="superusername" name="superusername" class="form-control" placeholder="Username" maxlength="30" required autofocus> 
                    </div> 
                </div> 
                <div class="form-group row"> 
                    <label for="superuserpassword" class="col-sm-3 col-md-2 col-form-label">Heslo:</label> 
                    <div class="col-sm-9 col-md-10"> 
                        <input type="password" id="superuserpassword" name="superuserpassword" class="form-control" placeholder="Password" maxlength="50" required> 
                    </div> 
                </div> 
                <div class="form-group row"> 
                    <div class="col"> 
                        <button class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" >Přihlásit se</button> 
                    </div>
                </div>
            </form> 
        </div>
    </div> 
</div>