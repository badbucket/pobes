<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-8">
                        <h1>
                            <strong><?php echo $record['name']; ?></strong><br>
                            <?php echo $title; ?>
                        </h1>
                    </div>
                    <div class="col">
                        <form class="form-inline company-login-form" action="<?php echo ASSETS; ?>/php/scripts/skolenilogout.php" method="post">
                            <button type="submit" class="btn btn-primary btn-light-blue" name="company-master-logout-form-submit">odhlásit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid proccess-nav-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 text-left">
                <span class="active">Krok 1. Výběr školení</span>
            </div>
            <div class="col-sm-12 col-md-4 text-center">
                <span>Krok 2. Vstupní informace</span>
            </div>
            <div class="col-sm-12 col-md-4 text-right">
                <span>Krok 3. Školení</span>
            </div>
        </div>
    </div>
</div>
<hr class="hr-black">
<div class="container select-question-group-container" style="min-height:750px">          
    <div class="row">
        <div class="col text-center ">
            <h3 class="instruction-text">Vyber typ školení</h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-7">
            <ol>
                <li>    
                    <a href="<?php echo ROOT . '/skoleni?testgroup=bozp'; ?>">
                        <h3 class="text-blue bold">BOZP</h3>
                        <p>Bezpečnost a ochrana zdraví při práci</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo ROOT . '/skoleni?testgroup=po'; ?>">
                        <h3 class="text-orange bold">PO</h3>
                        <p>Požární ochrana</p>
                    </a>
                </li>
                <li>
                    <!-- <a href=""> -->
                    <h3 class="text-grey bold">Školení řidičů referentů</h3>
                    <p>Školení řidičů služebních / firemních aut</p>
                    <!--</a>-->
                </li> 
            </ol>
        </div>
        <div class="col-3 text-center">
            <p class="">Online školení <strong>efektivně, rychle a z pohodlí domova.</strong></p>
            <img src="<?php echo ASSETS; ?>/img/skoleni.jpg" width="205" height="190" alt="Školení">
        </div>
    </div>
</div>