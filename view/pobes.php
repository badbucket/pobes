<div class="container container-imgnavbar"> 
    <p class="note-center-grey"><span class="bold text-blue">BOZP</span> a <span class="bold text-orange">PO</span> specialisté pro:</p>
    <div class="row justify-content-center text-center imgnavbar">
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-restaurace"><div></div><span>Restaurace, <br>kavárny</span></div>
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-hotel"><div></div><span>Hotely,<br>ubytovací zařízení</span></div>
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-nemocnice"><div></div><span>Zdravotnická<br>zařízení</span></div>
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-vzdelani"><div></div><span>Vzdělávací<br>zařízení, školy</span></div>
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-stavba"><div></div><span>Architekti,<br>stavebnictví</span></div>
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-vyroba"><div></div><span>Výroba</span></div>
        <div class="col-xs-4 col-md-2 col-xl-1 tab-toggle" id="tab-toggle-obchod"><div></div><span>Obchody,<br>prodejny</span></div>
    </div>
</div>
<div class="container-fluid main-content-container">
    <div class="row">
        <div class="col big-blue-center-note padded-spans-container">
            <span>Dotázat se - nezávazně poptat - objednat: </span>
            <span>pobes@pobes.cz</span><span class="no-mobile">/</span><span>+420 606 735 868</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="container text-center tab-tabs-container"> 
                <div class="tab-container">
                    <div class="row">
                        <div class="col text-grey">
                            <h2><span class="text-blue bold">BOZP a PO</span><span> - Zákonem dané povinnosti provozovatele - <strong></strong></span></h2>
                            <p class="h2-subheader padded-spans-min-container"><span>živnostník, s.r.o., a.s.</span><span>/</span><span>Dle aktuálně platného zákoníku práce 262/2006</span></p>
                        </div>
                    </div>
                    <div class="row" style="width: 100%;">
                        <div class="row justify-content-around text-left row-infolists">
                            <div class="col-sm-9 col-md-5">
                                <ol>
                                </ol>
                            </div>
                            <div class="col-sm-9 col-md-5">
                                <ol>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid container-note">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row justify-content-around text-left">
                    <div class="col-sm-12 col-md-5">
                        <p class="bold text-grey">+ Audit pro nové zákazníky ZDARMA!</p>
                    </div>
                    <div class="col-sm-9 col-md-5"></div>
                    <hr class="hr-black">
                </div>
                <div class="row justify-content-around text-left">
                    <div class="col-sm-12 col-md-5">
                        <p class="text-grey">Již máte zajištěného dodavatele služby BOZP a PO?</p>
                        <p class="text-blue bold">Nabídneme Vám lepší podmínky i cenu! (Garance nejnižší ceny)</p>
                    </div>
                    <div class="col-sm-9 col-md-5"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container margins-lg-td" id="reference">
    <div class="row margins-sm-td">
        <div class="col">
            <h2 class="note-center-grey">vybrané reference:</h2>
        </div>
    </div>
    <div class="row ref-img-row">
        <div class="col-sm-6 col-lg-3">
            <img src="<?php echo ASSETS ?>/img/ref/red_castle.jpg" alt="" width="229" height="98"> 
        </div>
        <div class="col-sm-6 col-lg-3">
            <img src="<?php echo ASSETS ?>/img/ref/otavske_strojirny.jpg" alt="" width="229" height="98">
        </div>
        <div class="col-sm-6 col-lg-3">
            <img src="<?php echo ASSETS ?>/img/ref/kartex.jpg" alt="" width="229" height="98">
        </div>
        <div class="col-sm-6 col-lg-3">
            <img src="<?php echo ASSETS ?>/img/ref/wikov.jpg" alt="" width="229" height="98">
        </div>
    </div>
    <hr class="hr-black">
    <div class="row ref-list-row margins-md-td">
        <div class="col-sm-6 col-lg-3">
            <ul>
                <li>Red Castle Hospitality Group a.s.</li>
                <li>Otavské strojírny a.s.</li>
                <li>KERAMIKA Helus s.r.o.</li>
                <li>KUFI INT, s.r.o.</li>
                <li>WOODGEN.TECH s.r.o.</li>
                <li>IRON PRODUCTION s.r.o.</li>
            </ul>
        </div>
        <div class="col-sm-6 col-lg-3">
            <ul>
                <li>Wikov Gear s.r.o.</li>
                <li>MASTNÝ <br>Architektonická projektová kancelář</li>
                <li>Farma Staré Sedlo s.r.o.</li>
                <li>HUMBOLTKA Ubytovna Nýřany s.r.o.</li>
                <li>KARTEX - požární ochrana staveb s.r.o.</li>
            </ul>
        </div>
        <div class="col-sm-6 col-lg-3">
            <ul>
                <li>GYSTOM, spol. s.r.o.</li>
                <li>Modrý zub MUDr. Nataša Kuralová</li>
                <li>M &amp; M DENT s.r.o.</li>
                <li>MUDr. Hana Böhmová</li>
                <li>MUDr. Jan Tichý</li>
                <li>MUDr. Jana Vyhnálková</li>
            </ul>
        </div>
        <div class="col-sm-6 col-lg-3">
            <ul>
                <li>ZŠ Heyrovského Plzeň</li>
                <li>ZŠ Podmostní Plzeň</li>
                <li>CRANIUM FUNDI s.r.o.</li>
                <li>a mnoho dalších ...</li>
            </ul>
        </div>
    </div>
</div>