<?php
if (!isLogged('companyUser')) {
    include('skoleniloginpage.php');
} else {
    $result = dibi::query('SELECT name FROM [Companies] WHERE companyId=%i', $_SESSION['companyUser']);
    $record = $result->fetch();
    if (isset($_GET['testgroup'])) {
        $selectedQuestionGroup = getQuestionGroup($_GET['testgroup']);
        if (!$selectedQuestionGroup) {
            include('skolenitestgrouppage.php');
        } else {
            $questions = makeTest($_GET['testgroup']);
            include('skolenipage.php');
        }
    } else {
        include('skolenitestgrouppage.php');
    }
}
?>
