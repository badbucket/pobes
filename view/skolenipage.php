<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row justify-content-center row-aligned-text">
                    <div class="col-8">
                        <h1>
                            <strong><?php echo $record['name']; ?></strong><br>
                            <?php echo $title; ?>
                        </h1>
                    </div>
                    <div class="col">
                        <form class="form-inline company-login-form" action="<?php echo ASSETS; ?>/php/scripts/skolenilogout.php" method="post">
                            <button type="submit" class="btn btn-primary btn-light-blue" name="company-master-logout-form-submit">odhlásit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid proccess-nav-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 text-left">
                <span>Krok 1. Výběr školení</span>
            </div>
            <div class="col-sm-12 col-md-4 text-center">
                <span class="active">Krok 2. Vstupní informace</span>
            </div>
            <div class="col-sm-12 col-md-4 text-right">
                <span>Krok 3. Školení</span>
            </div>
        </div>
    </div>
</div>
<hr class="hr-black">
<div class="container">   
    <div class="row justify-content-center">
        <div class="col">
            <h2 class="bold <?php echo $selectedQuestionGroup['name'] == 'BOZP' ? 'text-blue' : 'text-orange' ; ?>" style="margin-top: 10px;"><?php echo $selectedQuestionGroup['name']; ?></h2>
            <p class="text-grey"><?php echo $selectedQuestionGroup['description']; ?></p>
        </div>
        <div class="col userinfo-col text-grey hidden">
            <div><p><span class="bold"></span><br><span></span></p></div>
        </div>
    </div>
</div>
<hr class="hr-black">
<div class="container" style="min-height:750px">          
    <div class="row justify-content-center text-center">
        <div class="col-6">
            <form class="form-user-info" novalidate>
                <h3 class="instruction-text">Zadej vstupní údaje</h2>
                    <div class="form-group row">
                        <label for="name" class="col-3 col-form-label text-right text-grey">Jméno:</label>
                        <div class="col-9">
                            <input type="text" id="name" name="name" class="form-control" placeholder="" maxlength="100" pattern="[A-Za-z0-9\S]{1,25}" autofocus required>
                            <div class="invalid-feedback text-left">
                                <strong>Zadejte své jméno jako jedno slovo</strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-3 col-form-label text-right text-grey">Příjmení:</label>
                        <div class="col-9">
                            <input type="text" id="surname" name="surname" class="form-control" placeholder="" maxlength="100" pattern="[A-Za-z0-9\S]{1,25}" required>
                            <div class="invalid-feedback text-left">
                                <strong>Zadejte své příjmení jako jedno slovo</strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-3 col-form-label text-right text-grey">Místo výkonu povolání:</label>
                        <div class="col-9">
                            <select type="select" id="company" name="company" class="form-control" placeholder="" maxlength="100">
                            <?php
                                $result = dibi::query('SELECT name FROM [Companies] WHERE companyId=%i', $_SESSION['companyUser']);
                                $company = $result->fetch();
                                echo '<option value="' . $_SESSION['companyUser'] . '">' . $company['name'] . '</option>';
                                $subcompanies = getSubCompanies($_SESSION['companyUser']);
                                foreach ($subcompanies as $m => $row) {
                                    echo '<option value="' . $row['companyId'] . '">' . $row['name'] . '</option>';
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row download-select-container justify-content-center">
                        <div class="col"><img src="<?php echo ASSETS; ?>/img/pdf_file_icon.png" width="50" height="65" alt="Stáhnout PDF"><?php echo '<a target="_blank" href="' . DATA . '/studium/pdf/' . $selectedQuestionGroup['shortcut'] . '_studijni_text.pdf'; ?>"><span class="bold text-uppercase">STUDIJNÍ TEXT</span><span>Stáhnout a prostudovat</span></a></div>
                        <div class="col"><img src="<?php echo ASSETS; ?>/img/ppt_file_icon.png" width="62" height="64" alt="Stáhnout prezentaci"><?php echo '<a target="_blank" href="' . DATA . '/studium/ppt/' . $selectedQuestionGroup['shortcut'] . '_prezentace.ppt'; ?>"><span class="bold text-uppercase">ŠKOLÍCÍ PREZENTACE</span><span>Stáhnout a prostudovat</span></a></div>
                    </div>
                    <div class="form-group row text-left">
                        <div class="form-check">
                            <label class="form-check-label bold">
                                <input class="form-check-input checkbox-required" type="checkbox" required>
                                Potvrzuji, že jsem si prostudoval STUDIJNÍ TEXT a následně ŠKOLÍCÍ PREZENTACI (imístěné v modém poli výše) a jsem připravený na závěrečný test.
                            </label>
                        </div>
                    </div>
                    <div class="form-group row text-left">
                        <div class="form-check">
                            <label class="form-check-label bold">
                                <input class="form-check-input checkbox-required" type="checkbox" required>
                                Potvrzuji, že testové otázky zodpovím samostatně bez pomoci druhé osoby.
                            </label>
                        </div>
                    </div>
                    <p class="text-grey text-left italic">Tlačítko pro spuštění závěrečného testu se aktivuje po potvrzení nastudování školících podkladů</p>
                    <button type="submit" class="btn btn-lg btn-primary btn-block bold disabled" id="startTestButton" disabled>SPUSTIT ZÁVĚREČNÝ TEST</button>
            </form>
        </div>
    </div>
</div>
<div class="container question-container hidden" style="min-height:750px">
    <div class='row justify-content-center'>
        <div class='col'>
            <p><strong>Otázek celkem <span class="number-of-questions-span"></span>. (Zakřížkuj správnou odpověď. Po zadání správné odpovědi přejdeš automaticky na další otázku.)</strong></p>
            <br>
        </div>
    </div>
    <div class="row justify-content-center question-row hidden">
        <div class="col form-group question-content">
            <h2>Otázka č. <span class="question-counter">1</span></h2>
            <p class="question-text"></p>
            <div class="form-check answer-option">
                <label class="form-check-label">
                    <input class="form-check-input answer-option-radio" onchange="selectAnswer(event)" type="radio" name="exampleRadios" value="option1">
                    <span class="answer-text"></span>
                    <p class="alert-message bold"></p>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="container test-finished-message-container hidden text-center" style="min-height:750px">
    <div class='row justify-content-center'>
        <div class='col'>
            <h2 class="text-green">Blahopřejeme!</h3>
                <p>Úspěšně jste absolvovali online školení: <br>
                    <span class="question-group-name-span bold"></span> - 
                    <span class="question-group-description-span bold"></span>
                </p>
                <p>Údaje o vašem proškolení systém bezpečně uložil k ostatním dokumentům.</p>
                <p class="text-blue bold">Děkujeme Vám a přejeme úspěšný den.</p>
                <a href="<?php echo ROOT . '/skoleni'; ?>"><button class="btn btn-lg btn-primary">Vybrat další školení</button></a>
        </div>
    </div>
</div>