<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row justify-content-center row-aligned-text">
                    <div class="col">
                        <h1 class="">
                            <strong><?php echo $title; ?></strong> - <?php echo $subtitle; ?>
                        </h1>
                    </div>
                    <div class="col">
                        <form class="form-inline company-login-form" action="<?php echo ASSETS; ?>/php/scripts/dokumentylogin.php" method="post">
                            <label for="inlineFormCustomSelect">heslo</label>
                            <input type="password" class="form-control" name="companyMasterPassword" id="companyMasterPassword" required>
                            <button type="submit" class="btn btn-primary btn-light-blue" name="company-master-login-form-submit">potvrdit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container" style="min-height:750px;">
    <div class="row justify-content-center">
        <div class="col-3 text-center">
            <img src="<?php echo ASSETS; ?>/img/dokumenty.jpg" width="183" height="212" alt="Dokumenty">
        </div>
        <div class="col-7" style="display: grid;">
            <p class="notestyle" style="vertical-align: middle; margin: auto; margin-left: 0; font-size: 1.4rem;">
                BOZP a PO dokumentace na jednom místě.<br>
                <strong>Přehledně, bezpečně a dostupně zálohovaná dokumentace.</strong>
            </p>
        </div>
    </div>
</div>