<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row row-aligned-text">
                    <div class="col">
                        <h1 class="">
                            <strong><?php echo $title; ?></strong><br>
                            <?php echo $subtitle; ?>
                        </h1>
                    </div>
                    <div class="col-5 notestyle no-mobile">
                        <p>
                            Neváhejte se zeptat. Rádi Vám s čímkoliv poradíme:<br>
                            <strong>pobes@pobes.cz / +420 606 640 304</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <ul class="list-items chapters">
                <li><a href="#collapse1-headline">Obecné informace - Bezpečnost práce</a></li>
                <li><a href="#collapse2-headline">Dokumentace BOZP</a></li>
                <li><a href="#collapse3-headline">Školení BOZP</a></li>
                <li><a href="#collapse4-headline">Koordinátor BOZP na stavbě</a></li>
                <li><a href="#collapse5-headline">Plán BOZP</a></li>
                <li><a href="#collapse6-headline">Předpisy v oblasti BOZP</a></li>
                <li><a href="#collapse7-headline">Školení řidičů referentských vozidel ­- referentů</a></li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-6 grid-div">
            <img src="<?php echo ASSETS . '/img/bozp.jpg'; ?>" width="223" height="181" alt="BOZP - Bezpečnost a ochrana zdraví při práci">
        </div>
    </div>
</div>
<hr class="hr-black">
<div class="container">
    <div class="panel-group group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse1-headline" class="panel-title">
                    <a href="#collapse1-headline">Obecné informace - Bezpečnost práce</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Služby v oblasti BOZP</h5>
                    <p>Co vlastně potřebujete, ke splnění základních předpisů o bezpečnosti práce? Pro firmy nad 25 zaměstnanců je stanovena povinnost podle zák.č. 309/2006 Sb., že dokumenty, týkající se bezpečnosti práce, musí být konzultovány a podepsány odborně způsobilou osobou.</p>

                    <h5>Proč využívat externí firmu na zajištění všech služeb bezpečnosti práce?</h5>
                    <p>
                        <strong>Úspora nákladů</strong> – i když se to na první pohled nezdá, odborná firma zajistí komplexní služby v oblasti požární ochrany mnohdy levněji než vlastní zaměstnanec zajišťující BOZP. Je důležité si uvědomit zaměření odborné firmy a z toho vyplývající profesionalitu a orientaci v široké škále legislativních předpisů. V tomto ohledu je klientům poskytnuto bezchybné zajištění služeb BOZP, což je v mnoha směrech nedosažitelné prostřednictvím samotných zaměstnanců. Nedodržení předpisů pak může stát nemalé finanční prostředky.<br><br>
                        <strong>Jistota</strong> – externí odborná firma Vám zajistí předepsané periodické kontroly, školení a provede veškerá potřebná opatření k eliminaci rizikových faktorů.<br><br>
                        <strong>Poradenství</strong> - rychle dosažitelné odborné poradenství prostřednictvím e-mailu, popř. telefonu a osobní konzultace.
                    </p>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/obecne_informace_v_oblasti_bozp.pdf'; ?>">Podrobnější informace k BOZP</a>
                        </p>
                    </div>

                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/povinnosti_zamestnance.pdf'; ?>">Povinnosti zaměstnance</a>
                        </p>
                    </div>

                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/povinnosti_zamestnavatele.pdf'; ?>">Povinnosti zaměstnavatele</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse2-headline" class="panel-title">
                    <a href="#collapse2-headline">Dokumentace BOZP</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Informace k dokumentaci BOZP</h5>
                    <p><strong>Dokumentace BOZP</strong> vždy vychází z typu podnikání a záleží na spoustě faktorů jako je vybavení firmy – stroje a zařízení, počet zaměstnanců, obor podnikání a mnoho dalších souvisejících aspektů. Jednoduše řečeno <strong>dokumentace BOZP je vždy připravena přesně na míru posuzovanému směru podnikání</strong>. Na základě výše zmíněných údajů je jasné, že univerzální dokumentace BOZP neexistuje a že vytvoření správné a všem předpisům odpovídající dokumentace je velice složitá věc, která potřebuje odpovídající odborně způsobilou vzdělanou osobu.</p>

                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/seznam_dokumentace_bozp.pdf'; ?>">Seznam dokumentace BOZP</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse3-headline" class="panel-title">
                    <a href="#collapse3-headline">Školení BOZP</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Informace ke školení BOZP</h5>
                    <p><strong>Dle platného zákoníku práce a živnostenského zákona je zaměstnavatel povinen zajistit:</strong></p>
                    <ul>
                        <li>zaměstnancům školení o právních a ostatních předpisech k zajištění bezpečnosti a ochrany zdraví při práci, které doplňují jejich kvalifikační předpoklady a požadavky pro výkon práce, které se týkají jejich práce a pracoviště; pravidelně ověřovat jejich znalost a soustavně vyžadovat a kontrolovat jejich dodržování!</li>
                        <li>zaměstnancům, zejména zaměstnancům v pracovním poměru na dobu určitou, mladistvým a jejich zákonným zástupcům, podle potřeb vykonávané práce ve vhodných intervalech dostatečné a přiměřené informace a pokyny o bezpečnosti a ochraně zdraví při práci, zejména formou seznámení s riziky, s výsledky vyhodnocení rizik a s opatřeními na ochranu před působením těchto rizik, která se týkají jejich práce a pracoviště.</li>
                        <li>Základní školení zaměstnanců z předpisů se vztahem BOZP</li>
                        <li>Školení vedoucích pracovníků</li>
                        <li>Specializovaná školení</li>
                        <li>Organizace práce v prostředí s nebezpečím výbuchu NV 406/2004 Sb.</li>
                        <li>Stavebnictví – NV 591/2006 Sb.</li>
                        <li>Školení pro práci ve výškách dle NV 362/2005 Sb.</li>
                        <li>Bezpečná obsluha strojů</li>
                    </ul>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/podrobne_informace_o_skoleni_bozp.pdf'; ?>">Podrobnější informace ke školení BOZP</a>
                        </p>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse4-headline" class="panel-title">
                    <a href="#collapse4-headline">Koordinátor BOZP na stavbě</a>
                </h4>
            </div>

            <div id="collapse4" class="panel-collapse collapse in">
                <div class="panel-body">

                    <h5>Informace k činnosti koordinátora BOZP</h5>
                    <p>Se zákonem č. 309/2006 Sb., kterým se upravují další požadavky bezpečnosti a ochrany zdraví při práci v pracovněprávních vztazích, vznikla funkce <strong>"Koordinátor BOZP na staveništi"</strong>. Činnosti a povinnosti koordinátora jsou uvedeny v druhé a třetí části zákona. Zadavatelům staveb, které svým rozsahem splňují podmínky pro zajištění <strong>koordinátora BOZP</strong>, vznikají tímto zákonem další povinnosti.</p>

                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/cinnost_koordinatora_bozp.pdf'; ?>">Činnost koordinátora BOZP</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse5-headline" class="panel-title">
                    <a href="#collapse5-headline">Plán BOZP</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Informace a obsah plánu BOZP</h5>
                    <p>Plán BOZP zpracovává koordinátor BOZP. Jedná se o dokument obsahující údaje, informace a postupy zpracované v podrobnostech nezbytných pro zajištění bezpečné a zdraví neohrožující práce při realizaci stavby.</p>
                    <p>Plán BOZP je dokument, který se stává z textové a grafické části (příloh). Součástí plánu BOZP jsou informace o možných rizicích, přehled právních předpisů na úseku bezpečnosti práce na staveništi týkající se stavby. Rozsah celého plánu BOZP je dán velikostí a náročností celé stavby.</p>
                    <p><strong>Obsah plánu BOZP:</strong></p>
                    <ul>
                        <li>Základní informace o akci a účastnících výstavby,</li>
                        <li>povinnosti účastníků výstavby v oblasti zajištění BOZP,</li>
                        <li>přehled základních opatření k zajištění BOZP,</li>
                        <li>vymezení činností, rozsahu prací a stanovení pracovních postupů a odpovědností,</li>
                        <li>analýzy rizik jednotlivých činností,</li>
                        <li>způsob hlášení mimořádných událostí a pracovních úrazů,</li>
                        <li>zásady požární ochrany při realizaci,</li>
                        <li>dopravně provozní předpisy,</li>
                        <li>zabezpečení staveniště,</li>
                        <li>bezpečnost práce při udržovacích pracích při užívání stavby.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse6-headline" class="panel-title">
                    <a href="#collapse6-headline">Předpisy v oblasti BOZP</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse in">
                <div class="panel-body">
                    <h5>Informace k předpisům v oblasti BOZP</h5>
                    <p>Právní úprava bezpečnosti a ochrany zdraví při práci (BOZP) je velmi rozsáhlá a zahrnuje celou řadu zákonů, nařízení vlády a vyhlášek, jak je vidět z následujícího ustanovení § 349 zákona č. 262/2006 Sb., zákoníku práce, který je tím základním předpisem:</p>
                    <p><i>"Právní a ostatní předpisy k zajištění bezpečnosti a ochrany zdraví při práci - BOZP jsou předpisy na ochranu života a zdraví, předpisy hygienické a protiepidemické, technické předpisy, technické dokumenty a technické normy, stavební předpisy, dopravní předpisy, předpisy o požární ochraně a předpisy o zacházení s hořlavinami, výbušninami, zbraněmi, radioaktivními látkami, chemickými látkami a chemickými přípravky a jinými látkami škodlivými zdraví, pokud upravují otázky týkající se ochrany života a zdraví."</i></p>
                    <div class="download-document">
                        <p>
                            <a target="_blank" href="<?php echo DATA . '/dokumentace/seznam_predpisu_bozp.pdf'; ?>">Seznam předpisů BOZP</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 id="collapse7-headline" class="panel-title">
                    <a href="#collapse7-headline">Školení řidičů referentských vozidel ­- referentů</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p><i>"Referentské školení, neboli školení řidičů referentů, je školení řidičů (zaměstnanců), kteří při výkonu svého zaměstnání řídí motorová vozidla, ale nespadají přitom do režimu
                            „Profesního školení“.</i></p>
                    <p>Povinnost účastnit se referentského školení řidičů je ukotvena v zákoníku práce (č.262/2006 Sb,  V § 103 odst. 2) a týká se všech řidičů, kteří v souvislosti s výkonem
                        zaměstnání řídí motorové vozidlo. Jedná se například o zaměstnance, kteří řídí vozidlo na služební cestě bez ohledu, zda se jedná o vozidlo služební nebo soukromé ale také o
                        zaměstnance, který řídí vozidlo na účelové komunikaci v uzavřeném prostoru nebo objektu (areál, sklad, atd.).</p>
                    <p>Zákoník práce v § 103 odst. 2 ukládá zaměstnavateli povinnost „zajistit zaměstnancům školení o právních a ostatních předpisech k zajištění bezpečnosti a ochrany zdraví při
                        práci, které doplňují jejich odborné předpoklady a požadavky pro výkon práce, které se týkají jimi vykonávané práce a vztahují se k rizikům, s nimiž může přijít zaměstnanec do
                        styku na pracovišti, na kterém je práce vykonávána, a soustavně vyžadovat a kontrolovat jejich dodržování“.</p>
                    <p>Zaměstnavatel má právo určit osobu, která bude mít školení na starosti. Vždy by to měla být osoba odborně způsobilá (OZO), která je poradcem, metodikem, organizátorem a
                        také koordinátorem pro oblast BOZP.</p>
                </div>
            </div>
        </div>
    </div> 
</div>