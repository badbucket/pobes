    <div class="container font-smaller container-subnav">
        <div class="row" style="margin-bottom: 15px;">
            <div class="col no-mobile"> 
                <div class="div-aligned-content">
                    <div>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#addCompanyForm" aria-expanded="false" aria-controls="addCompanyForm">
                            Přidat firmu
                        </button>
                    </div>
                </div>
                <form class="collapse add-company-form" id="addCompanyForm">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="subjectTypeRadios" value="addCompany" checked>
                            Přidat firmu
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="subjectTypeRadios" value="addBranch">
                            Přidat pobočku / vyber firmu
                        </label>
                    </div>
                    <div class="form-group"> 
                        <select class="form-control" name="companyNameSelect" id="companyNameSelect" disabled> 
                            <option disabled selected value> -- select an option -- </option> 
                            <?php 
                            foreach(getParentCompanyNames() as $companyId => $name) { 
                                echo '<option value="' . $companyId . '">' . $name . '</option>'; 
                            } 
                            ?> 
                        </select> 
                    </div>
                    <div class="form-group" style="margin-top: 25px;">
                        <label name="name">Název</label>
                        <input type="text" name="name" class="form-control" value="<?php echo $company['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">Popisek</label>
                        <input type="text" name="description" class="form-control" value="<?php echo $company['description']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">Město</label>
                        <input type="text" name="city" class="form-control" value="<?php echo $company['city']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">Ulice</label>
                        <input type="text" name="street" class="form-control" value="<?php echo $company['street']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">č.p.</label>
                        <input type="text" name="houseNumber" class="form-control" value="<?php echo $company['houseNumber']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">s.č.</label>
                        <input type="text" name="serialNumber" class="form-control" value="<?php echo $company['serialNumber']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">ičo</label>
                        <input type="text" name="ico" class="form-control" value="<?php echo $company['ico']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">Kontaktní osoba</label>
                        <input type="text" name="contactName" class="form-control" value="<?php echo $company['contactName']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">Email</label>
                        <input type="text" name="email" class="form-control" value="<?php echo $company['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">Telefon</label>
                        <input type="text" name="phoneNumber" class="form-control" value="<?php echo $company['phoneNumber']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">AUDIT</label>
                        <input type="date" name="nextCheckupDate" class="form-control" value="<?php echo $company['nextCheckupDate']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">H-vstup</label>
                        <input type="text" name="masterAccessCode" class="form-control" value="<?php echo $company['masterAccessCode']; ?>">
                    </div>
                    <div class="form-group">
                        <label name="name">H-školení</label>
                        <input type="text" name="accessCode" class="form-control" value="<?php echo $company['accessCode']; ?>">
                    </div>
                    <div class="form-group" style="margin-top: 25px;">
                        <button type="button" name="addCompany" class="form-control company-button-add btn btn-lg btn-success">Vytvořit</button>  
                    </div>
                </form>
            </div>
            <div class="col div-aligned-content">
                <div class="col"> 
                    <p>Počet firem a poboček: <strong><?php echo getNumberOfCompanies(); ?></strong></p> 
                </div> 
                <div class="col" style="padding-right: 0;"> 
                    <a class="simple-link" href="<?php echo ASSETS; ?>/php/scripts/adminlogout.php"><button class="btn btn-primary btn-light-blue">odhlásit</button></a>
                </div> 
            </div> 
        </div>
    </div>
    <div class="container-fluid font-smaller">  
        <div class="col unpaded-mobile">
            <table class="table-companies table table-hover table-sortable tablesorter resizable"> 
                <thead> 
                    <tr> 
                        <th>zák. č.</th> 
                        <th class="no-mobile">rok</th> 
                        <th>Název</th> 
                        <th class="no-mobile">Popisek</th> 
                        <th class="no-mobile">Město</th> 
                        <th class="no-mobile">Ulice</th> 
                        <th class="no-mobile">č.p.</th> 
                        <th class="no-mobile">s.č.</th> 
                        <th class="no-mobile">IČO</th>
                        <th class="no-mobile">kontaktní osoba</th> 
                        <th class="no-mobile">telefon</th> 
                        <th class="no-mobile">email</th> 
                        <th class="text-center no-mobile">H-vstup</th> 
                        <th class="text-center no-mobile">H-školení</th> 
                        <th class="text-center no-mobile">Š-<?php echo date('y', strtotime("-1 year")) ?></th> 
                        <th class="text-center no-mobile">Š-<?php echo date('y') ?></th> 
                        <th class="text-center">AUDIT</th> 
                        <th class="text-center">Hlídač</th> 
                        <th class="text-center no-mobile">DOC</th> 
                        <th class="text-center no-mobile">UPRAV</th>
                    </tr> 
                </thead> 
                <tbody> 
                    <?php 
                    $result = getParentCompanies();
                    foreach ($result as $m => $row) { 
                        $companyCounter = $m + 1;
                        echo '<tr subjecttype="company" subjectid="' . $row["companyId"] . '" dropdown-toggle>'; 
                        echo '  <td class="uneditable-cell">' . $companyCounter . '</td>'; 
                        echo '  <td class="no-mobile">' . (is_object($row["timeCreated"]) ? $row["timeCreated"]->format("Y") : '') . '</td>'; 
                        echo '  <td class="bold">' . $row["name"] . '</td>'; 
                        echo '  <td class="no-mobile">' . $row["description"] . '</td>'; 
                        echo '  <td class="no-mobile">' . $row["city"] . '</td>'; 
                        echo '  <td class="no-mobile">' . $row["street"] . '</td>'; 
                        echo '  <td class="no-mobile">' . $row["houseNumber"] . '</td>'; 
                        echo '  <td class="no-mobile">' . $row["serialNumber"] . '</td>'; 
                        echo '  <td class="bold">' . $row["ico"] . '</td>';
                        echo '  <td class="bold no-mobile">' . $row["contactName"] . '</td>'; 
                        echo '  <td class="bold no-mobile">' . $row["phoneNumber"] . '</td>'; 
                        echo '  <td class="bold no-mobile">' . $row["email"] . '</td>'; 
                        echo '  <td class="text-center no-mobile">' . $row["masterAccessCode"] . '</td>'; 
                        echo '  <td class="text-center no-mobile">' . $row["accessCode"] . '</td>'; 
                        echo '  <td class="text-center no-mobile">' . getNumberOfTestsInYear($row["companyId"], date('Y', strtotime("-1 year"))) . '</td>'; 
                        echo '  <td class="text-center no-mobile">' . getNumberOfTestsInYear($row["companyId"], date('Y')) . '</td>'; 
                        if (is_object($row['nextCheckupDate'])) { 
                            echo '<td class="td-date';
                            if (date('Y-m-d', strtotime($row["nextCheckupDate"])) <= date("Y-m-d", strtotime("+2 month"))) {
                                if (date('Y-m-d', strtotime($row["nextCheckupDate"])) <= date("Y-m-d", strtotime("+1 month"))) {
                                    if (date('Y-m-d', strtotime($row["nextCheckupDate"])) <= date('Y-m-d', strtotime('now'))) {
                                        echo ' background-red';
                                    }
                                    else {
                                        echo ' text-red';
                                    }
                                }
                                else {
                                    echo ' text-blue';
                                }
                            }
                            echo '"><span>' . $row["nextCheckupDate"]->format("d. m. Y") . '</span><span class="hidden">' . $row["nextCheckupDate"]->format("Y-m-d") . '</span></td>';
                        } 
                        else { 
                            echo '<td class="td-date-null text-center"><span class="text-grey">NE</span><span class="hidden">3000-01-01</span></td>'; 
                        }
                        $record = getClosestDateDocument($row['companyId']);
                        if (is_object($record['dateValid'])) {
                            echo '<td class="td-date';
                            if (date('Y-m-d', strtotime($record["dateValid"])) <= date('Y-m-d', strtotime('+2 month'))) {
                                if (date('Y-m-d', strtotime($record["dateValid"])) <= date('Y-m-d', strtotime('+1 month'))) {
                                    if (date('Y-m-d', strtotime($record["dateValid"])) <= date('Y-m-d', strtotime('now'))) {
                                        echo ' background-red';
                                    }
                                    else {
                                        echo ' text-red';
                                    }
                                }
                                else {
                                    echo ' text-blue';
                                }
                            }
                            echo '"><span>' . $record['dateValid']->format('d. m. Y') . '</span><span class="hidden">' . $record['dateValid']->format('Y-m-d') . '</span></td>';
                        }
                        else {
                            echo '<td class="td-date-null text-center"><span class="text-grey">NE</span><span class="hidden">3000-01-01</span></td>'; 
                        }
                        echo '  <td class="text-center no-mobile"><a href="' . ROOT .  '/admin?company=' . $row["companyId"] . '&document' . '"><button type="button" name="addCompanyDocument">' . 'DOC' . '</button></a></td>'; 
                        echo '  <td class="text-center no-mobile"><a href="' . ROOT .  '/admin?company=' . $row["companyId"] . '"><button type="button" class="company-button-edit">' . 'UPRAV' . '</button></a></td>'; 
                        echo '</tr>'; 
                        $subresult = getSubCompanies($row["companyId"]); 
                        foreach ($subresult as $n => $subrow) { 
                            $subCompanyCounter = $n + 1;
                            echo '<tr subjecttype="branch" subjectid="' . $subrow["companyId"] . '">'; 
                            echo '  <td class="uneditable-cell">' . $companyCounter . '/' . $subCompanyCounter . '</td>'; 
                            echo '  <td class="no-mobile">' . (is_object($subrow["timeCreated"]) ? $subrow["timeCreated"]->format("Y") : '') . '</td>'; 
                            echo '  <td class="text-blue"><div class="first-half">' . $companyCounter . '/' . $subCompanyCounter . '</div><div class="bold second-half">' . $subrow["name"] . '</div></td>';
                            echo '  <td class="no-mobile">' . $subrow["description"] . '</td>'; 
                            echo '  <td class="no-mobile">' . $subrow["city"] . '</td>'; 
                            echo '  <td class="no-mobile">' . $subrow["street"] . '</td>'; 
                            echo '  <td class="no-mobile">' . $subrow["houseNumber"] . '</td>'; 
                            echo '  <td class="no-mobile">' . $subrow["serialNumber"] . '</td>'; 
                            echo '  <td class="no-mobile">' . $subrow["ico"] . '</td>'; 
                            echo '  <td class="bold no-mobile">' . $subrow["contactName"] . '</td>'; 
                            echo '  <td class="bold no-mobile">' . $subrow["phoneNumber"] . '</td>'; 
                            echo '  <td class="bold no-mobile">' . $subrow["email"] . '</td>'; 
                            echo '  <td class="text-center no-mobile">' . $subrow["masterAccessCode"] . '</td>'; 
                            echo '  <td class="text-center no-mobile">' . $subrow["accessCode"] . '</td>'; 
                            echo '  <td class="text-center no-mobile">' . getNumberOfTestsInYear($subrow["companyId"], date('Y', strtotime("-1 year"))) . '</td>'; 
                            echo '  <td class="text-center no-mobile">' . getNumberOfTestsInYear($subrow["companyId"], date('Y')) . '</td>'; 
                            if (is_object($subrow['nextCheckupDate'])) { 
                                echo '<td class="td-date';
                                if (date('Y-m-d', strtotime($subrow["nextCheckupDate"])) <= date("Y-m-d", strtotime("+2 month"))) {
                                    if (date('Y-m-d', strtotime($subrow["nextCheckupDate"])) <= date("Y-m-d", strtotime("+1 month"))) {
                                        if (date('Y-m-d', strtotime($subrow["nextCheckupDate"])) <= date('Y-m-d', strtotime('now'))) {
                                            echo ' background-red';
                                        }
                                        else {
                                            echo ' text-red';
                                        }
                                    }
                                    else {
                                        echo ' text-blue';
                                    }
                                }
                                echo '"><span>' . $subrow["nextCheckupDate"]->format("d. m. Y") . '</span><span class="hidden">' . $subrow["nextCheckupDate"]->format("Y-m-d") . '</span></td>';
                            } 
                            else { 
                                echo '<td class="td-date-null text-center"><span class="text-grey">NE</span><span class="hidden">3000-01-01</span></td>'; 
                            }
                            $record = getClosestDateDocument($subrow['companyId']);
                            if (is_object($record['dateValid'])) { 
                                echo '<td class="td-date';
                                if (date('Y-m-d', strtotime($record["dateValid"])) <= date('Y-m-d', strtotime('+2 month'))) {
                                    if (date('Y-m-d', strtotime($record["dateValid"])) <= date('Y-m-d', strtotime('+1 month'))) {
                                        if (date('Y-m-d', strtotime($record["dateValid"])) <= date('Y-m-d', strtotime('now'))) {
                                            echo ' background-red';
                                        }
                                        else {
                                            echo ' text-red';
                                        }
                                    }
                                    else {
                                        echo ' text-blue';
                                    }
                                }
                                echo '"><span>' . $record['dateValid']->format('d. m. Y') . '</span><span class="hidden">' . $record['dateValid']->format('Y-m-d') . '</span></td>';
                            }
                            else {
                                echo '<td class="td-date-null text-center"><span class="text-grey">NE</span><span class="hidden">3000-01-01</span></td>'; 
                            }   
                            echo '  <td class="text-center no-mobile"><a href="' . ROOT .  '/admin?company=' . $subrow["companyId"] . '&document' . '"><button type="button" name="addCompanyDocument">' . 'DOC' . '</button></a></td>';
                            echo '  <td class="text-center no-mobile"><a href="' . ROOT .  '/admin?company=' . $subrow["companyId"] . '"><button type="button" class="company-button-edit">' . 'UPRAV' . '</button></a></td>'; 
                            echo '</tr>'; 
                        } 
                    } 
                    ?> 
                </tbody> 
            </table>
        </div> 
        </div> 
    </div>
</div>
<style>
    #myModal * {
        font-size: 12px;
    }
    #myModal table {
        margin: auto;
        width: 100%;
    }
    #myModal table td:first-child {
        font-style: italic;  
    }
    #myModal table td:last-child {
        font-weight: bold;  
    }
</style>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!--<h4 class="modal-title">Modal Header</h4>-->
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default text-center" data-dismiss="modal">Zavřít</button>
        </div>
    </div>  
</div>
</div> 
