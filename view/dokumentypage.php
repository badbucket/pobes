<hr class="hr-black">
<div class="container" style="margin-top: 17px; margin-bottom: 17px;">
    <div class="row justify-content-between">
        <div class="col-5">
            <h4 class="text-blue" style="margin-bottom: 0">BOZP</h4>
        </div>
        <div class="col-5">
            <h4 class="text-orange" style="margin-bottom: 0">PO</h4>
        </div>
    </div>
</div>
<hr class="hr-black">
<div class="container text-grey" style="margin-top: 34px;">
    <div class="row row-company-documents justify-content-between">
        <div class="col-5">
            <h5>BOZP Dokumentace</h5>
            <?php
            $resultDocs = dibi::query('SELECT * FROM [Documents] WHERE companyId=%i AND type="bozp"', $company['companyId']);
            $docs = $resultDocs->fetchAll();
            echo('<ul class="list-items list-documents">');
            foreach ($docs as $doc) {
                echo('<a class="text-grey link-icon-pre" target="_blank" href="' . DATA . '/uloziste' . '/' . $doc['documentId'] . '_' . $doc['nameNorm'] . '"><li><img src="' . ASSETS . '/img/pdf_file_icon_white.png" width="16" height="21" alt="Stáhnout PDF"> ' . $doc['name']);
                if (is_object($doc['dateValid'])) {
                    echo(' / <span');
                    if ($doc['dateValid'] < new DateTime) {
                        echo(' style="color:red;font-weight:bold"');
                    }
                    else {
                        echo(' style="color:green;font-weight:bold"');
                    }
                    echo('>' . $doc['dateValid']->format("d. m. Y") . '</span>');
                }
                echo('</li></a>');
            }
            echo('</ul>');
            ?>
        </div>
        <div class="col-5">
            <h5>PO Dokumentace</h5>
            <?php
            $resultDocs = dibi::query('SELECT * FROM [Documents] WHERE companyId=%i AND type="po"', $company['companyId']);
            $docs = $resultDocs->fetchAll();
            echo('<ul class="list-items list-documents">');
            foreach ($docs as $doc) {
                echo('<a class="text-grey link-icon-pre" target="_blank" href="' . DATA .'/uloziste' . '/' . $doc['documentId'] . '_' . $doc['nameNorm'] . '"><li><img src="' . ASSETS . '/img/pdf_file_icon_white.png" width="16" height="21" alt="Stáhnout PDF"> ' . $doc['name']);
                if (is_object($doc['dateValid'])) {
                    echo(' / <span');
                    if ($doc['dateValid'] < new DateTime) {
                        echo(' style="color:red;font-weight:bold"');
                    }
                    else {
                        echo(' style="color:green;font-weight:bold"');
                    }
                    echo('>' . $doc['dateValid']->format("d. m. Y") . '</span>');
                }
                echo('</li></a>');
            }
            echo('</ul>');
            ?>
        </div>
    </div>
    <div class="row row-company-documents justify-content-between">
        <div class="col-5">
            <h5>BOZP školení - zaměstnanci</h5>
            <?php
            $resultTests = dibi::query('SELECT testId, name, submittedBy, testDate FROM [Tests] WHERE companyId=%i AND name="bozp"', $company['companyId']);
            $tests = $resultTests->fetchAll();
            echo('<ul class="list-items list-documents">');
            foreach ($tests as $test) {
                echo('<a class="text-grey link-icon-pre" target="_blank" href="' . ASSETS . '/php/scripts/downloadtest.php?id=' . $test['testId'] . '"><li');
                echo('><img src="' . ASSETS . '/img/pdf_file_icon_white.png" width="16" height="21" alt="Stáhnout PDF"> ' . $test['submittedBy'] . '<span class="text-blue"> / ');
                echo($test['testDate']->format("d. m. Y") . '</span></li></a>');
            }
            echo('</ul>');
            ?>
        </div>
        <div class="col-5">
            <h5>PO školení - zaměstnanci</h5>
            <?php
            $resultTests = dibi::query('SELECT testId, name, submittedBy, testDate FROM [Tests] WHERE companyId=%i AND name="po"', $company['companyId']);
            $tests = $resultTests->fetchAll();
            echo('<ul class="list-items list-documents">');
            foreach ($tests as $test) {
                echo('<a class="text-grey link-icon-pre" target="_blank" href="' . ASSETS . '/php/scripts/downloadtest.php?id=' . $test['testId'] . '"><li');
                echo('><img src="' . ASSETS . '/img/pdf_file_icon_white.png" width="16" height="21" alt="Stáhnout PDF"> ' . $test['submittedBy'] . '<span class="text-blue"> / ');
                echo($test['testDate']->format("d. m. Y") . '</span></li></a>');
            }
            echo('</ul>');
            ?>
        </div>
    </div>
</div>