<div class="container font-smaller container-subnav">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col no-mobile"> 
            <div class="div-aligned-content">
                <div>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#addDocumentForm" aria-expanded="false" aria-controls="addDocumentForm">
                        Přidat dokument
                    </button>
                </div>
            </div>
            <form class="collapse add-document-form" id="addDocumentForm" enctype="multipart/form-data" action="<?php echo ASSETS; ?>/php/scripts/uploadtoserver.php?id=<?php echo $company['companyId']; ?>" method="POST">
                <div class="form-group">
                    <input type="file" name="uploaded"></input> 
                </div> 
                <div class="form-group">
                    Datum platnosti: <input type="date" name="validdate"></input>
                </div> 
                <div class="form-check">
                    Hlídač: <input type="checkbox" name="dateguard" checked></input>
                </div> 
                <div class="form-group">
                    Kategorie: 
                    <select name="type">
                        <option value="bozp">bozp</option>
                        <option value="po">po</option>
                    </select> 
                </div> 
                <div class="form-group"> 
                    <button type="submit" name="addDocument" class="document-button-add btn btn-lg btn-success">Nahrát</button> 
                </div>
            </form>
        </div>
        <div class="col"> 
            <form class="form-inline company-login-form">
                <a class="simple-link" href="<?php echo PROTOCOL.DOMAIN . '/admin'; ?>"><button type="button" class="btn btn-primary btn-light-blue">ZPĚT</button></a>
            </form>    
        </div> 
    </div>
</div>
<div class="container-fluid font-smaller">  
    <div class="col unpaded-mobile">
        <div class="form-row" style="min-height:500px;"> 
            <div class="col">
                <table class="table-documents table table-hover table-sortable tablesorter resizable"> 
                    <thead> 
                        <tr> 
                            <th name="name">Název</th>
                            <th name="dateCreated" class="text-center">Vytvořeno</th> 
                            <th name="dateValid" type="date" class="text-center" editable>Platnost</th> 
                            <th name="type" type="text" class="text-center" editable>Kategorie</th> 
                            <th name="dateGuard" class="text-center" switchable>Hlídač</th>
                            <th class="text-center">SMAZAT</th>
                        </tr> 
                    </thead> 
                    <tbody> 
                        <?php 
                        $result = getCompanyDocuments($company['companyId'], true); 
                        foreach ($result as $m => $row) { 
                            echo '<tr>'; 
                            echo '  <td>' . $row["name"] . '</td>'; 
                            
                            if (is_object($row['dateCreated'])) { 
                                echo '<td class="td-date"><span>' . $row['dateCreated']->format('d. m. Y') . '</span><span class="hidden">' . $row['dateCreated']->format('Y-m-d') . '</span></td>';
                            }
                            
                            if (is_object($row['dateValid'])) { 
                                echo '<td class="td-date';
                                if ($row['dateGuard'] === 1 && date('Y-m-d', strtotime($row['dateValid'])) <= date('Y-m-d', strtotime('+2 month'))) {
                                    if (date('Y-m-d', strtotime($row['dateValid'])) <= date('Y-m-d', strtotime('+1 month'))) {
                                        if (date('Y-m-d', strtotime($row['dateValid'])) <= date('Y-m-d', strtotime('now'))) {
                                            echo ' background-red';
                                        }
                                        else {
                                            echo ' text-red';
                                        }
                                    }
                                    else {
                                        echo ' text-blue';
                                    }
                                }
                                echo '"><span>' . $row['dateValid']->format('d. m. Y') . '</span><span class="hidden">' . $row['dateValid']->format('Y-m-d') . '</span></td>';
                            } 
                            else { 
                                echo '<td class="td-date-null text-center"><span class="text-grey">NE</span><span class="hidden">3000-01-01</span></td>'; 
                            } 

                            echo '  <td class="text-center">' . $row['type'] . '</td>'; 
                            
                            echo '  <td class="text-center">' . ($row['dateGuard'] === 1 ? 'ANO' : 'NE') . '</td>'; 
                            
                            echo '  <td class="text-center"><button type="button" class="document-button-delete" value="' . $row['documentId'] . '">' . 'SMAZAT' . '</button></td>';
                            
                            echo '</tr>';
                        } 
                        ?> 
                    </tbody> 
                </table>
            </div> 
        </div>
    </div>
</div>  
