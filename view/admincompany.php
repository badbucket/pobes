<div class="container font-smaller container-subnav">
    <div class="row">
        <div class="col">
            <form class="form-inline company-login-form">
                <a class="simple-link" href="<?php echo PROTOCOL.DOMAIN . '/admin'; ?>"><button type="button" class="btn btn-primary btn-light-blue">ZPĚT</button></a>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <form id="editCompanyForm" accept-charset='UTF-8'>
        <div class="form-row margins-sm-td font-smaller">
            <div class="form-group col">
            <h3>Firma</h3>
                <table class="table update-company-table resizable">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Popisek</th>
                            <th>Město</th>
                            <th>Ulice</th>
                            <th>č.p.</th>
                            <th>s.č.</th>
                            <th>ičo</th>
                            <th>AUDIT</th>
                            <th>H-vstup</th>
                            <th>H-školení</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text" name="name" class="form-control" value="<?php echo $company['name']; ?>"></td>
                            <td><input type="text" name="description" class="form-control" value="<?php echo $company['description']; ?>"></td>
                            <td><input type="text" name="city" class="form-control" value="<?php echo $company['city']; ?>"></td>
                            <td><input type="text" name="street" class="form-control" value="<?php echo $company['street']; ?>"></td>
                            <td><input type="text" name="houseNumber" class="form-control" value="<?php echo $company['houseNumber']; ?>"></td>
                            <td><input type="text" name="serialNumber" class="form-control" value="<?php echo $company['serialNumber']; ?>"></td>
                            <td><input type="text" name="ico" class="form-control" value="<?php echo $company['ico']; ?>"></td>
                            <td><input type="date" name="nextCheckupDate" class="form-control" value="<?php echo $company['nextCheckupDate'] ? $company['nextCheckupDate']->format('Y-m-d') : ''; ?>"></td>
                            <td><input type="text" name="masterAccessCode" class="form-control" value="<?php echo $company['masterAccessCode']; ?>"></td>
                            <td><input type="text" name="accessCode" class="form-control" value="<?php echo $company['accessCode']; ?>"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-row margins-sm-td font-smaller">
            <div class="form-group col">
                <h3>Kontaktní osoba</h3>
                <table class="table update-contact-person-table resizable">
                    <thead>
                        <tr>
                            <th>Jméno</th>
                            <th>Email</th>
                            <th>Telefon</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text" name="contactName" class="form-control" value="<?php echo $company['contactName']; ?>"></td>
                            <td><input type="text" name="email" class="form-control" value="<?php echo $company['email']; ?>"></td>
                            <td><input type="text" name="phoneNumber" class="form-control" value="<?php echo $company['phoneNumber']; ?>"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-row justify-content-center margins-sm-td">
            <div class="form-group col-xs-12 col-sm-5" style="margin: auto;">
                <button type="button" name="saveChanges" id="companyUpdateButton" class="company-button-update btn btn-lg btn-success" value="<?php echo $_GET['company']?>">Uložit změny</button></a>
            </div>
        </div>
        <div id="companyDeleteButtonContainer" class="form-row justify-content-center margins-sm-td">
            <div class="form-group col-xs-12 col-sm-3" style="margin: auto;">
                <button type="button" name="deleteCompany" id="companyDeleteButton" class="company-button-delete btn btn-lg btn-danger" <?php echo 'value="' . $company['companyId'] . '"'; ?>>ODSTRANIT</button>
            </div>
        </div>
        <div id="companyDeleteConfirmationDialog" class="margins-sm-td" style="display: none;">
            <div class="row">
                <div class="col text-center instruction-text">
                    Určitě chcete tuto firmu odstranit?
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="form-group col-xs-12 col-sm-1">
                    <button type="button" class="btn btn-lg btn-success fill" value="yes">ANO</button>
                </div>
                <div class="form-group col-xs-12 col-sm-1">
                    <button type="button" class="btn btn-lg btn-danger fill" value="no">NE</button>
                </div>
            </div>
        </div>
    </form>
</div>