<div class="container-fluid container-big-blue">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row justify-content-center row-aligned-text">
                    <div class="col">
                        <h1 class="page-title-header bold"><?php echo $title; ?></h1>
                    </div>
                    <div class="col">
                        <form class="form-inline company-login-form" action="<?php echo ASSETS; ?>/php/scripts/skolenilogin.php" method="post">
                            <label for="inlineFormCustomSelect">heslo</label>
                            <input type="password" class="form-control" name="companyUserPassword" id="companyUserPassword" required>
                            <button type="submit" class="btn btn-primary btn-light-blue" name="company-user-login-form-submit">potvrdit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container online-test-logo-container" style="min-height:750px">
    <div class="row justify-content-center">
        <div class="col text-center">
            <p class="notestyle">Online školení <strong>BOZP, PO a Školení řidičů referentů</strong> efektivně, rychle a z pohodlí domova.</p>
            <img src="<?php echo ASSETS; ?>/img/skoleni.jpg" width="205" height="190" alt="Školení">
        </div>
    </div>
</div>