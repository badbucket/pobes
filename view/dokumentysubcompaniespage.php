<?php
$cities = array();
foreach ($companies as $m => $row) {
    if (!in_array($row['city'], $cities)) {
        array_push($cities, $row['city']);
    }
}
echo '<div style="min-height: 700px; margin-top: 30px;">';
foreach ($cities as $m => $city) {
    echo '<div class="container">';
    echo '  <div class="row justify-content-center">';
    echo '    <div class="col">';
    echo '      <h3 class="bold" style="margin-bottom:2px;">' . $city. '</h3>';
    echo '    </div>';
    echo '  </div>';
    echo '</div>';
    echo '<hr class="hr-black" style="margin-top:4px;margin-bottom:4px;">';
    echo '<div class="container">';
    echo '  <div class="row justify-content-center">';
    echo '    <div class="col">';
    echo '      <ul class="list-items list-subcompanies">';
    foreach ($companies as $n => $row) {
        if ($row['city'] === $city) {
            echo '<li><a class="text-grey link-grey" href="' . PROTOCOL.DOMAIN . '/dokumenty?id=' . $row['companyId'] . '"><span>' . $row['name'] . '</span><span> - ' . $row['street'] . ' ' . $row['houseNumber'] . '</span></a></li>';
        }
    }
    echo '      </ul>';
    echo '    </div>';
    echo '  </div>';
    echo '</div>';
}
echo '</div>';
?>