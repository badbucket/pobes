<?php
if (isset($_GET['company'])) {
    if ($company = getCompanyById($_GET['company'])) {
        if (isset($_GET['document'])) {
            include('admindocuments.php');
        }
        else {
            include('admincompany.php');
        }
    }
    else {
        include('admincompanies.php');
    }
}
else {
    include('admincompanies.php');
}