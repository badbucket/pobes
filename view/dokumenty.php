<?php
if (!isLogged('companyMaster')) {
    include('dokumentyloginpage.php');
}
else {
    $company = getCompanyById($_SESSION['companyMaster']);
    $companies = getSubCompanies($company['companyId']);
    $companyIds = array_map(create_function('$o', 'return $o->companyId;'), $companies);
    if (isset($_GET['id']) && $_GET['id'] !== $_SESSION['companyMaster'] && in_array($_GET['id'], $companyIds)) {
        $isSubcompany = true;
        $company = getCompanyById($_GET['id']);
        $companies = getSubCompanies($company['companyId']);
    }
    include('dokumentyheader.php');
    if (sizeof($companies) == 0 || $_GET['id'] == $_SESSION['companyMaster']) {
        include('dokumentypage.php');
    } else {
        include('dokumentysubcompaniespage.php');
    }
}